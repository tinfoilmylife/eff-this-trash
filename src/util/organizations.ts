export interface Organization {
  name: string;
  logo: string;
  imgHeight: number;
  description: string;
  link: string;
}

export const organizations: Organization[] = [
  {
    name: 'World Econmic Forum (WEF)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/wef%20(1).webp',
    imgHeight: 300,
    description:
      '\u201cYou will own nothing and you will be happy.\u201d This infamous phrase was coined by the World Economic Forum and featured in a short video they published that was quickly removed because of the backlash. You probably don\u0027t want to live in a world where you rent everything and own nothing, and no one would be able to build any sort of wealth in such a world. This is EXACTLY what the WEF wants for us all, among many other nefarious things. The WEF is an organization that has built a massive network on ultra-wealthy corporations and people, all of which are working together to ensure that they own and control everything so we will own nothing and have no freedom. Lead by Klaus Schwab who can best be described as a Bond super-villain, the WEF has an annual meeting in Davos each year where they talk about things like putting devices into medications that send a message to them to ensure your compliance in taking these meds, pacifying the \u201cuseless eaters\u201d with drugs and video games as AI makes more people unemployed, censoring \u201cdis-\u201d and \u201cmis-\u201d information, creating a social credit score system in which we would all be forced to participate, and much more. They have no direct power as an organization, but they have a massive amount of influence on every single entity that is a member of their large organization and are working to force their globalist agenda onto all of us. The \u201cGreat Reset\u201d of which they commonly speak is nothing more than destroying our ways of life, economies, and social norms so the companies that are members can have complete control of every aspect of our existence. This is why we should avoid supporting any company that is a member of the WEF whenever possible.',
    link: 'https://www.weforum.org/',
  },
  {
    name: 'Black Lives Matter (BLM)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/blm%20(1).webp',
    imgHeight: 249,
    description:
      'In the Kid Rock song \u201cWe the People\u201d, there is a line where he says \u201cBlack live matter, no sh!t motherf**ker\u201d. It is safe to assume that the VAST majority of us agree with this statement and believe that all lives matter regardless of skin color, race, or nationality. The organization called Black Lives Matter is a corrupt organization fueling the flames of real racism, pushing for the victimhood/oppressor mentality and CRT to be taught to children, inciting of riots creating billions of dollars of damage and destruction in our cities, encouraging the defunding of and hatred towards the police, and so many more terrible things. Like so many other organizations in this list, BLM takes an idea that has almost 100% agreement with people and mutates it into something that is leveraged to create violence and unrest, futhers the divide among everyone, and gives them a healthy amount of wealth. Speaking of wealth, look into how BLM spends their money. It is difficult to find any examples of them attempting to help the black community as they claim, but you can find many examples of how the leaders of BLM are giving themselves million dollar bonuses while they buy large mansions. This organization is nothing more than a race grift that is creating chaos in the world and they should not receive support from anyone.',
    link: 'https://blacklivesmatter.com/',
  },
  {
    name: 'Blackrock',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/BlackRock%20(1).webp',
    imgHeight: 225,
    description:
      'BlackRock owns everything. Are your medications from Pfizer? BlackRock owns them, which means they were a very large catalyst for the vaccine mandates and \u201canti-vax\u201d censorship the past three years, all for profit and control. Do you watch TV? BlackRock owns a large portion of the media outlets so you can bet your last buck that none of those outlets would ever say anything negative about BlackRock, their subsidiaries, or companies in which they have large investments. Do you use Google or Apple products? BlackRock owns large portions of these companies and MANY, MANY others which means they have a massive amount of influence over how these companies operate. If there are rental properties in your neighborhood, BlackRock probably owns them as well as they seem to be trying to create a world in which we cannot own our homes but must rent them from BlackRock. This evil company is understandably referred to by many as the most evil company on the planet. It has been said that they own something around $12 trillion in assets, although the exact number seems impossible to find. They are also a part of the WEF, created and enforce ESG, and so much more. Any company pushing wokeness, talking about ESG and DEI, etc. are doing so either because they are owned by BlackRock, have large investments from BlackRock, or hope to eventually have BlackRock invest in them. This insanely evil company owns too much and has too much power. They also seem to be attempting to make sure that we own nothing and have no power at all.',
    link: 'https://www.blackrock.com/us/individual/about-us/about-blackrock',
  },
  {
    name: 'Human Rights Campaign (HRC)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/hrc%20(1).webp',
    imgHeight: 210,
    description:
      'The Human Rights Campaign (HRC) is the largest alphabet people lobbying group. Like so many other things on this list, we have an organization with a mission statement that is seemingly good and that everyone can agree with, but they take it to a dark and evil place. The HRC publishes articles with insanely far-left biases that feed Trump derangement syndrome, further the political divide in this country, and basically attack anyone that is not as far-left as they are. They are also strongly in favor of not only labeling any speech they don\u0027t like as \u201chate speech\u201d, but are actively pushing for more hate speech laws for things like \u201cmisgendering\u201d when those ideas have not been accepted by the well over half of the country that aren\u0027t far-leftists. Many also blame the HRC for pushing the ESG, DEI, and CEI programs into American corporations.',
    link: 'https://www.hrc.org/',
  },
  {
    name: 'World Health Organization (WHO)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/who%20(1).webp',
    imgHeight: 225,
    description:
      'The World Health Organization (WHO) is a corrupt, globalist organization that is attempting to gain a lot more power these days. For starters, they are currently trying to get 194 countries to sign a pandemic treaty that would give them complete control of the governments of these countries in the event they declare another pandemic. It is said that this treaty would even give the WHO more power than The Constitution in the US allowing them to force \u201cvaccines\u201d onto Americans, impose lockdowns without government support, and more. The WHO was highly criticised for their handling of the pandemic early on as they essentially let China mishandle the whole situation in the beginning, making it possible for the virus to spread rapidly across the globe. This along with the fact that they have covered for and praised the CCP\u0027s handling of COVID early on have led many to believe that they are working in China\u0027s interests. The WHO also has ties to the WEF.',
    link: 'https://www.who.int/',
  },
  {
    name: 'Anti-Defimation League (ADL)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/adl%20(1).webp',
    imgHeight: 187,
    description:
      'The Anti-Defamation League (ADL): here we go again with yet another organization that with a virtuous mission that is really just a far left activist group and tool for the globalists to use. It is said that the ADL works closely with the FBI and is responsible for the corruption, indoctrination, and heavy politicalization of the FBI which is no longer respected among most circles as continued evidence of their corruption and weaponisation comes to light. The ADL also seems to be in strong support of open borders, hate speech laws, teaching critical race theory in schools, getting rid of the electoral college, reparations, and many other initiatives that really do nothing more than stoke division and hatred while targeting anyone that isn\u0027t a far leftist.',
    link: 'https://www.adl.org/',
  },
  {
    name: 'Food and Drug Administration (FDA)',
    description:
      'The U.S. Food and Drug Administration is supposed to exit to “protect the public health by ensuring the safety, efficacy, and security of human and veterinary drugs, biological products, and medical devices; and by ensuring the safety of our nation\u0027s food supply, cosmetics, and products that products that emit radiation.” That sounds noble and you can\u0027t argue that we don’t need something like this to exist. The biggest problem with this organization is that the companies it regulates fund it through user fees. In other words, let\u0027s say that Pfizer REALLY wants something to be approved, but that drug had a lot of terrible side effects during the trials. Big pharma companies make up for about 75% if the FDA’s drug division funding. Don\u0027t pretend, for a second, that the FDA has enough integrity to not let the fact that Pfizer and others pay millions of dollars a year to the FDA sway their ruling on certain issues. Of course, that less-than-safe new drug that Pfizer can charge 1000x markup for is getting approved. The institution approving it has become corrupt at their core. The FDA also regulates our food supply and is supposed to keep food items safe as well, but there are several hundred chemicals they allow our food manufacturers to use that are banned from food in almost all other countries. Money seems to be all the FDA is concerned about these days, and this is allowing the companies we are supposed to be protected from to buy the “protectors” who, in turn, allow for unsafe food and drug items to be on the market. This is why our food is full of endocrine disruptors, microplastics, high fructose corn syrup, and worse.',
    imgHeight: 225,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/fda.webp',
    link: 'https://www.fda.gov/',
  },
  {
    name: 'National Highway Traffic Safety Administration (NHTSA)',
    description:
      'The NHTSA is yet another example of an organization created for the purposes of protecting the consumer, and it this case it is the car owner. Like many other government alphabet agencies, the NHTSA has become nothing more than a corrupt institution bought and paid for by corporations to do their bidding. With Biden in office, this has become even worse. There was a Data Access Law in Massachusetts that would allow vehicle owners to access their own ODB data from the vehicles they purchased for the sake of diagnosing and repairing their own vehicles. This law had auto manufacturers scrambling and releasing a series of hyperbolic ads to argue that it is unsafe to allow this access. Virtually every major auto manufacturer was involved here, so we can\u0027t point out one who is the most anti-right-to-repair as they all seem to be that way. It looked like the auto manufacturers were going to lose, but then in June 2023, the NHTSA sent out a letter that basically told them not to worry about the law if they were concerned about car hacking. Well, they will say that is their concern, even though it is of little to no real threat to anyone, just to be able to ignore the law. The result from this is a complete monopoly on auto repair, all thanks to the NHTSA. If you have car problems, you better be prepared to pay a huge markup and have a huge wait to get your repair done at the dealership because that is the future the NHTSA wants for us as it is the future they have received tons of lobbying money from auto manufacturers to create.',
    imgHeight: 222,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/nhtsa.webp',
    link: 'https://www.nhtsa.gov/',
  },
  {
    name: 'US Securities and Exchange Commission (SEC)',
    description:
      'Here we go again. \u201cThe SEC protects investors in the $3.8 trillion municipal securities markets that cities and towns rely on to provide neighborhood schools, local libraries and hospitals, public parks, safe drinking water and so much more.\u201d No, you protect the 0.1%, Wall Street jerks, etc. The SEC rarely sues individual defendants at large financial institutions, settling instead with the entity only. When it does sue individual defendants, it frequently loses. The penalties collected by the commission from corporate defendants are declining and, in any event, are modest in proportion to the profits obtained. In other words, mega-wealthy companies and individuals are allowed to break all sorts of financial laws and get a slap on the wrist. The very thing that the SEC exists from which to protect the people is the thing it really never does or does well. Additionally, the SEC is also the primary entity going after cryptocurrency. Say what you will about cryptocurrency, but the truth is that crypto represents a decentralized monetary system where the currency actually has a real world value as opposed to paper money which only has value as we agree it does and which becomes less valuable every day. Crypto empowers people to make anonymous transactions, build wealth more quickly, and do many other things that threatens to destroy the death grip that the central banking system has on all things financial. If you haven\u0027t figured it out yet, our governments have been purchased by the incredibly wealthy banking class, and they can\u0027t stand the thought of losing the control they have on everything. The SEC is supposed to protect us some from that and better regulate and punish the ultra wealthy and bankers for their wrongdoings in the market, but that no longer seems to be something of interest to them. They basically exist to destroy crypto at this point and nothing else.',
    imgHeight: 400,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/sec.webp',
    link: 'https://www.sec.gov/',
  },
  {
    name: 'National Institutes of Health (NIH)',
    description:
      'Are we noticing a trend that government alphabet agencies are corrupt at their core? The NIH is no exception! It looks like they were funding some of the scientists at the Wuhan lab from which COVID almost certainly was created and leaked. Back in 2021, the NIH stopped funding bat coronavirus research, only to begin funding the same research yet again in 2023. It seems as if all US government organizations that claim to exist for our health and safety are the ones putting our health and safety at risk! It also appears that more than $350 million of undisclosed payments were to Fauci, Francis Collins, and other top NIH scientists. In fact, it appears that 60% of these undisclosed payments happened right before COVID began and up to 1,675 scientists received payments. I wonder what these payments were for? Where did they come from? We\u0027ll never know for sure, but we deserve to know. A logical assumption would be that these payments had to do with COVID in some way, whether it be for going along with the narrative or hush money or who knows what. We also have evidence to support the idea that pharma companies have made payments to members of the NIH and that the NIH had a big hand in the mishandling of COVID and almost certainly its creation.',
    imgHeight: 145,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/nih.webp',
    link: 'https://www.nih.gov/',
  },
  {
    name: 'Centers for Disease Control (CDC)',
    description:
      'Oh, the CDC. There certainly wasn\u0027t as much awareness around this organization before 2020, but everyone knows who they are now. The CDC is responsible for the social distancing precautions, which we now know were completely pulled out of thin air with no scientific data to support them and that did absolutely nothing to prevent the spread of COVID. Social distancing created a lot of chaos, cause people to miss the funerals of loved ones, and much more. The CDC also supported masks and mask mandates, and we know have evidence from Israeli researchers that conclude that masks only have a 0.2% chance of stopping the spread of a virus. Those masks mandates caused division, hypoxia, conflict, speech development issues in small children, and more. Remember being told to take 2 experimental, unproven, and dangerous jabs. Then a booster. Then many more boosters? Guess who supported all of this trash? If the CDC made a statement about COVID, many people treated that as the gospel. This sad situation allowed a corrupt organization of unelected bureaucrats to do the bidding of the globalists, big pharma, and banking class that seemed to be trying to condition us to give up our constitutionally protect freedoms and accept being locked down and ruled by edict. What should be an organization concerned with the health of the citizens has apparently become an organization that uses their influence to push the agendas of those with deep enough pockets. Stop caring about what these assholes say. They don’t care about you at all and they have lost all credibility.',
    imgHeight: 285,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/cdc.webp',
    link: 'https://www.cdc.gov/',
  },
  {
    name: 'Federal Bureau of Investigations (FBI)',
    description:
      'The FBI has been accused of so many wrongdoings in the past few years that they could have an entire page dedicated to them. At this point, anyone that argues that the FBI has NOT been weaponized and politicized is an NPC with their head in the sand. There have been reports that the FBI attempted to purge any conservative agents, because that sounds like something a federal policing agency should be doing. If you need documents to be lost or hidden, much like ol\u0027 China Joe, the FBI can certainly help you with that task. Maybe you want your top political rival and ex-president to be unjustly investigated for major crimes, much like you would see in a banana republic country? The FBI is here to help with that as well! Whether you need a political hit job, cover up, the disappearance of someone, a rival to be \u201csuicided\u201d, or whatever terribly illegal and immoral thing done to ensure that your corruption and pedophilia are not exposed, the FBI is the organization you need on your side!  ',
    imgHeight: 225,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/fbi.webp',
    link: 'https://www.fbi.gov/',
  },
  {
    name: 'Internal Revenue Service (IRS)',
    description:
      'The IRS has become the militarized and weaponized arm of the Democratic party. Whether it is destroying 30 million tax records (according to the Treasury Inspector General) to cover up who knows what, or it is an unlawful raid on a Montana gun store where they seized paperwork to buy gun from tens of thousands of sales, none of which had pricing or tax related info on them, the IRS does whatever it wants to serve those who pull their strings. Recently, there was funding approved for 87,000 new IRS agents and they are evidently receiving weapons training. They\u0027re about to come after us in the middle class. Also, if you have a transaction of over $600 through an app like Venmo, be prepared for the IRS to request you claim that on your taxes. For as long as I can remember, $1,200 has been the cutoff for reporting, and it makes zero sense that they would lower the number as the dollar keeps losing value. Maybe they hired those 87k new agents to also enforce the carbon taxes that the globalists are salivating at the idea of imposing if they can manage to make CDBC\u0027s a reality? Who knows, but the IRS has always been hated, although they have never deserved that hatred more than they do now.',
    imgHeight: 225,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/irs.webp',
    link: 'https://www.irs.gov/',
  },
  {
    name: 'Department of Transportation (DOT)',
    description:
      'Imagine a world where, even though we are more technologically advanced than ever and everyone at the top claims that we are on the verge of some BS climate crisis, we still have an alarming number of trains derail and they always spill toxic chemicals into the environment or atmosphere. That\u0027s our DOT! To make matters worse, the train company involved with some of those derailings was basically unpunished, as any insignificant dollar amount the DOT fined them is the equivalent of asking a regular person for a penny. If you do a search for the history of corruption within the DOT, you will find a countless number of articles and example of people in higher positions accepting bribes and kickbacks throughout the years. The organization in charge of building and maintaining our bridges, railways, etc. seems completely content with bridges collapsing and trains going off the rails. They can\u0027t be bothered with those issues as they have to, instead, talk about how equity is their top priority. These people are demons!',
    imgHeight: 210,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/dot.webp',
    link: 'https://www.transportation.gov/',
  },
  {
    name: 'Bureau of Alcohol, Tobacco, and Firearms (ATF)',
    description:
      'The ATF cannot make laws, but that didn\u0027t stop them from trying to enforce a rule they came up with banning pistol braces which made tens of millions of Americans into felons overnight. Also, people with physical disabilities need pistol braces to shoot a pistol, and the ATF\u0027s rule said that they could only keep their braces if they registered them with the ATF which is unconstitutional. Essentially, the ATF has become an organization of lawless jackboot thugs. There are countless stories about how someone bought two or three guns in short succession and the ATF shows up on their doorstep soon after trying to intimidate them and asking questions they have no business asking. Look at things through this lens; the Democrats and globalists want to take away our guns, but the constitution stands in their way. The ATF is being used to make it more difficult to obtain guns and to scare those of us who wish to do so. It also appears as if they have developed a habit of confiscating gun transfer records from FFLs to potentially build a gun registry which, again, is unconstitutional. Speaking of FFLs, the ATF seems to be auditing them more frequently and with a greater than ever intimidation factor. This is yet another example of how they are attempting to make it more difficult to purchase a firearm and their actions are making many FFLs question whether the hassle is worth it to continue on with their business. Bottom line, the ATF is already coming for your guns!',
    imgHeight: 222,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/atf.webp',
    link: 'https://www.atf.gov/',
  },
  {
    name: 'Anything that Bill Gates touches',
    description:
      'Bill Gates. Before I get into this one, there will be a lot of \u201callegedly\u201d statements as this man is, in my opinion, and evil, corrupt, power-hungry, litigious, ego-maniacal billionaire that allegedly had an unhealthy friendship with Epstein who was exploiting minors sexually. Gates is famous for creating Microsoft, the company known for creating Windows which was, allegedly, an idea stolen from others. Windows is poorly architected, which makes it so vulnerable to viruses, and the last 2 major versions are riddled with so much telemetry that Microsoft probably knows you better than your parents and spouse do at this point. This billionaire didn\u0027t stop at just having tech money. No, he seems to be friends with another Bond supervillain, Klaus Schwab of the WEF. He also has proposed firing something into the atmosphere to block out the sun, purchased more farmland than any other person in the US (potentially to control the food supply), backed lab-grown meat companies (this stuff is poison for your body), spoken about reducing the global population, allegedly made billions from the COVID jabs he was peddling telling alleged lies regarding their safety and efficacy, and so much more. Gates, like so many other psychopaths with all the money, has also pushed the climate change lie to justify his actions and to be leveraged for more control. Oh yeah, did you ever hear anything about the experimental vaccines pushed to African countries that sterilized and killed many African people? Gates was, allegedly, behind that mess as well. Many of the charitable donations he has made to trick people into thinking he is a philanthropist were to his own organizations or the organizations owned by other wealthy people from which Gates likely wanted something, allegedly. Lastly, Gates had been testing the usage of genetically modified mosquitoes in Florida and Texas to deliver a malaria vaccine. Now we have cases of malaria in Texas and Florida for the first time in several decades, and guess who has backed yet another pharma company that just developed a new form of a malaria vaccine? Billy boy is one soy filled, moobs having, evil sack is shit, and nothing he touches seems even a little bit clean at this point (allegedly)! Also, Epstein didn\u0027t kill himself!',
    imgHeight: 292,
    logo: 'https://ubrecoco.sirv.com/Images/organizations/gates.webp',
    link: '',
  },
];

/**
 * DOJ
 */
