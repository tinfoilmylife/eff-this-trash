export const sortBy = (data: any[], attr: string) => {
  return data.sort((a: any, b: any) => {
    const aVal = a[attr];
    const bVal = b[attr];

    if (aVal > bVal) {
      return 1;
    }
    if (aVal < bVal) {
      return -1;
    }
    return 0;
  });
};
