import { Category, CategoryObj } from './constants.ts';

interface ReasonItem {
  text: string;
  link: string;
}

export interface Reason {
  main: ReasonItem;
  extra?: ReasonItem[];
}

export interface CompanyLink {
  href: string;
  label: string;
}

export interface GoodWebsite {
  link: string;
  domain: string;
}

export interface Company {
  name: string;
  logo?: string | null;
  reasons: Reason[];
  links?: CompanyLink[];
  alternatives?: string[];
  tags: string[];
  subsidaries?: string[];
  categories: string[];
  score: number;
  website?: GoodWebsite;
}

export type CategoryT =
  | 'racist'
  | 'politics'
  | 'anti-consumer'
  | 'alphabet'
  | 'kids'
  | 'demonic';

export type GoodCategoryT =
  | 'apolitical'
  | 'environment'
  | 'foss'
  | 'freespeech'
  | 'madeInUsa'
  | 'patriotic'
  | 'privacy'
  | 'repair';

export interface FeatureTextSectionLink {
  link: string;
  linkText: string;
}

export interface BadFilter extends Category {
  selected?: boolean;
}

export interface BadFilterObj {
  alphabet: BadFilter;
  antigun: BadFilter;
  'anti-consumer': BadFilter;
  censorship: BadFilter;
  demonic: BadFilter;
  kids: BadFilter;
  politics: BadFilter;
  privacy: BadFilter;
  racist: BadFilter;
  woke: BadFilter;
}

export interface FiltersItem {
  display: string;
  name: string;
  selected: boolean;
}

export interface ColumnData {
  key: string;
  format: keyof typeof formatters;
}
