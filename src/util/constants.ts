export interface Category {
  name: string;
  display: string;
  src: string;
  alt: string;
  description: string;
  selected?: boolean;
}

export interface CategoryObj {
  alphabet: Category;
  antigun: Category;
  'anti-consumer': Category;
  censorship: Category;
  demonic: Category;
  kids: Category;
  politics: Category;
  privacy: Category;
  racist: Category;
  woke: Category;
}

export interface GoodCategoryObj {
  apolitical: Category;
  environment: Category;
  foss: Category;
  freespeech: Category;
  madeInUsa: Category;
  patriotic: Category;
  privacy: Category;
  repair: Category;
}

export interface JointCatObj
  extends Partial<CategoryObj>,
    Partial<GoodCategoryObj> {}

export const categoriesList: CategoryObj = {
  alphabet: {
    name: 'alphabet',
    display: 'Pushes LGBT Agenda',
    src: 'https://ubrecoco.sirv.com/Images/categories/alphabet.webp',
    alt: 'Rainbow flag',
    description:
      'The \u201cPushes LGBT Agenda\u201d category is intended to indicate that the company in question tends to push the LGBT stuff a bit too hard. Most people don\u0027t care if you want to be gay, and it has generally been accepted for many years at this point. Gay couples can get married, file taxes together, buy property together, adopt a child, have their insurance policies cover their partner, etc so many people wonder while all the LGBT stuff still has to be constantly shoved down our throats as equal rights already exist. The vast majority of us take SERIOUS issue with the idea of exposing children to these ideas at an inappropriate age, but that is specifically covered by another category which is \u201ctargets children inappropriately\u201d. Also, why refer to this as \u201cthe LGBT agenda\u201d and not use the whole acronym? No real disrespect is meant by that, but the official acronym has changed so many times over the years that is has become difficult to remember and has gotten quite long so it is just easier and shorter to say \u201cthe LGBT agenda\u201d at this point. It is also starting to feel like the \u201cT\u201d does not belong and is probably its own movement all together, but that is a topic of discussion for another day.',
  },
  antigun: {
    name: 'antigun',
    display: 'Anti-2A',
    src: 'https://ubrecoco.sirv.com/Images/categories/antigun.webp',
    alt: 'gun inside circle with slash through it',
    description:
      'The \u201cAnti-2A\u201d category is intended to indicate that the company in question tends to constantly show support for doing away with the second amendment. Whether they censor, deplatform, or ban creators for posting 2A related content or lobby for unconstitutional anti-2A laws, constantly post anti-gun rhetoric, etc these companies really want guns to disappear. They either don\u0027t understand that criminals would always have guns at this point regardless of whether they are illegal or they simply don\u0027t want us law abiding gun owners to be capable of defending ourselves since we are mostly in disagreement with them on 99.9% of every other topic. If you care about being able to defend yourself and your family, don\u0027t support these companies!',
  },
  'anti-consumer': {
    name: 'anti-consumer',
    display: 'Anti-Consumer',
    src: 'https://ubrecoco.sirv.com/Images/categories/greed.webp',
    alt: 'dollar sign',
    description:
      'The \u201cAnti-Consumer\u201d category is intended to indicate that the company in question tends to have malicious, greedy, and outrageous practices that are terrible for the consumer. The idea of true ownership is being attacked and the companies that fall into this category are why. Some of these companies are anti-right-to-repair meaning that their practices or policies are designed to make sure that you either cannot repair a product you purchase from them or it can only be repaired at a substantial markup at an official repair center. Other companies in this category may just have greedy practices like installing the hardware for heated seats into a vehicle you purchase but making you pay a subscription fee for those heated seats to function (BMW). Whatever the case may be here, any company deemed \u201canti-consumer\u201d hates their customers and acts in pure greed.',
  },
  censorship: {
    name: 'censorship',
    display: 'Censorship',
    src: 'https://ubrecoco.sirv.com/Images/categories/censorship.webp',
    alt: 'circle with slash through it',
    description:
      'The \u201cCensorship\u201d category is intended to indicate that the company in question tends to support censorship. In modern society, we have a real problem with governments and corporations being overly censorious under the false pretense of protecting everyone from \u201cmisinformation\u201d or \u201cdisinformation\u201d. There seems to be a common pattern of powerful entities virtue signaling to \u201cprotect us\u201d when they are really trying to protect themselves, their bottom line, and their interests while attempting to retain control of the narrative. First and foremost, adults do not need protection from speech and ideas. Additionally, all this ever amounts to is the censorship of \u201cwrong think\u201d. Free speech is important to societal and personal growth, as we can have no growth in a hive mind where only one idea about a certain topic is allowed. This is why the first amendment exists, and this is also why we should do whatever we can to protect it!',
  },
  demonic: {
    name: 'demonic',
    display: 'Satanic/Demonic',
    src: 'https://ubrecoco.sirv.com/Images/categories/demon.webp',
    alt: 'demon',
    description:
      'The \u201cSatanic/Demonic\u201d category is intended to indicate that the company in question is just plain evil in some way. We exist in a world where you can be attacked and discriminated against for publicly professing your faith if you are Christian, Jewish, Muslim, etc, but somehow we are supposed to be ok with companies openly supporting evil ideas that are 100% at odds with every single faith in existence outside of Satanism?  Whether it is a retailer that is selling shirts with Molock on them (Target) or a TV channel having an awards ceremony with sacrilegious performances (CBS for having the demonic Sam Smith performance at the Grammys), these companies seem to take no issue with the idea of offending people of multiple faiths while putting absolute evil out into the world.',
  },
  kids: {
    name: 'kids',
    display: 'Targets Children Inappropriately',
    src: 'https://ubrecoco.sirv.com/Images/categories/children.webp',
    alt: 'boy and girl silhouettes inside of circle',
    description:
      'The \u201cTargets Children Inappropriately\u201d category is intended to indicate that the company in question is targeting children in inappropriate ways. Let kids remain innocent and be kids, for crying out loud! Why is it acceptable to sell LGBT agenda attire for children when children shouldn\u0027t be thinking of sexuality at all? How is it that we don\u0027t let kids vote, buy tobacco or alcohol, purchase a firearm, get a tattoo or piercing, enter into a legally binding contract, etc but we have companies supporting the permanent mutilation of the bodies or children under the guise of \u201chealth care\u201d? Most people support the idea of \u201clive and let live\u201d and take no issue with grown adults making whatever decision they wish as long as that decision does not infringe upon the rights of others. That said, almost no one should be ok with pushing these ideas onto children, so we need to stick it to the companies that support and push these ideas to children at ridiculously inappropriate ages.',
  },
  politics: {
    name: 'politics',
    display: 'Political Discrimination',
    src: 'https://ubrecoco.sirv.com/Images/categories/politics.webp',
    alt: 'speech bubble with courthouse icon inside',
    description:
      'The \u201cPolitical Discrimination\u201d category is intended to indicate that the company in question has discriminated against people or other organizations for political reasons. We need political discourse between the two sides of the aisle now more than ever to stop the ever-growing divide that currently exists from becoming more extreme. Instead, we appear to have many companies that think it is ok to attack, censor, ban/shadow ban, etc people for no other reason than they disagree with them politically. This is not only illegal, but it is immoral and has already caused substantial damage to our society. While the idea of supporting an organization because you agree with them politically is absolutely fine, the idea of companies attempting to silence or even harm the wellbeing of other people and organizations over political ideologies is completely unacceptable and should be punished. After all, a company is not a person and should simply STFU and provide a product or service, not engage in political discussions and abuse their status to stifle anyone of any ideology.',
  },
  privacy: {
    name: 'privacy',
    display: 'Violates Privacy',
    src: 'https://ubrecoco.sirv.com/Images/categories/privacy.webp',
    alt: 'human eye looking forward',
    description:
      'The \u201cViolates Privacy\u201d category is intended to indicate that the company in question has terrible privacy practices. Whether they are known to have predatory terms and conditions that include selling of your data to several 3rd parties, their site or app has a large amount of trackers, or they are insecure and have had multiple data breaches, doing business with these companies means you are being tracked, traced, and databsed and that data WILL eventually be leaked or purchased. ',
  },
  racist: {
    name: 'racist',
    display: 'Actually Racist',
    src: 'https://ubrecoco.sirv.com/Images/categories/racism.webp',
    alt: 'hands of different races joined together',
    description:
      'The \u201cActually Racist\u201d category is intended to indicate that the company in question has done something that is truly racist. The terms \u201cracist\u201d and \u201cracism\u201d are thrown around too lightly these days as you can be the recipient of one of those labels for merely disagreeing with someone on any topic. People should not be discriminated against regardless of what race they are. Almost every race has been a slave or owned slaves, and we ended slavery in the USA over 160 years ago. Additionally, only 1.6% of people owned slaves in the USA at the peak of slavery so that vast majority of us are not from a lineage that owned or were slaves. Even if we are, it all ended so long ago that it is not the fault of anyone alive now. If a company is calling for reparations or racial justice, likes to make social media posts about racial issue, or just simply do something that is actually racist, it will fall into this category and we should not be giving them our money! ',
  },
  woke: {
    name: 'woke',
    display: 'General Wokeness',
    src: 'https://ubrecoco.sirv.com/Images/categories/woke.webp',
    alt: 'keyboard key with W on it',
    description:
      'The \u201cGeneral Wokeness\u201d category is a bit of a catch-all category to indicate that a company may not have committed any single offense that is large enough to land them in another category or care too much about on its own, but has done many woke things over a period of time. Wokeness is a mind virus that is destroying the world in which we live. It is not appropriate for companies to be so vocal about political, social, and societal issues. Additionally, the majority of people believe that a meritocracy is the ideal system where individuals are hired or enrolled in a program based on their qualifications rather than fulfilling diversity, equity, and inclusion criteria. Let\u0027s do our part in curing this woke mind virus by hurting the companies that love to spread it so much!',
  },
};

export const scoresWithDesc = [
  {
    score: 1,
    description:
      'The offenses committed by a company with this score are mild and annoying at worst and rarely warrant a boycott unless their actions are regarding a topic about which you\u0027re particularly passionate or you are going all in with the boycotts. Avoiding companies with this score is not a bad idea, but going the extra mile to use alternative/competitors is probably not worth it for most people. This could change at any point, though.',
  },
  {
    score: 2,
    description:
      'The offenses committed by a company with this score are annoying and a slight bit serious in some way. A company with this score will be worth a boycott to a bit more people than companies with a lower score, but none of the offenses are that egregious on their own…yet.',
  },
  {
    score: 3,
    description:
      'The offenses committed by a company with this score have crossed the line of being simply \u201cannoying\u201d and are a bit more serious. Boycotts make sense for many of these companies as a message needs to be sent to them, but if you have to do business with them because of lack of options, then you don\u0027t need to feel too bad about it. This is the \u201cavoid if you can\u201d score.',
  },
  {
    score: 4,
    description:
      'The offenses committed by a company with this score have crossed a serious line or 2 or 5. If a company manages to receive this score, they\u0027ve been pushing agendas, saying stupid things on social media, etc for a long time and has some doozies that have made big waves. This is the \u201cmake the effort to avoid these companies\u201d score as widespread boycotts absolutely make sense here.',
  },
  {
    score: 5,
    description:
      'The offenses committed by a company with this score have made it clear that they hate us all and don\u0027t want our money. Companies with this score need to be boycotted at whatever the cost. Sacrifices should be made and self control exercised as, if a company manages the highest score on here, they are the worst of the worst of the worst as far as their actions are concerned. Go out of your way to avoid these companies.',
  },
];

export const goodCategories: GoodCategoryObj = {
  apolitical: {
    name: 'apolitical',
    display: 'Apolitical',
    src: 'https://ubrecoco.sirv.com/Images/categories/apolitical.webp',
    alt: 'Democrat and Republican logos crossed out',
    description:
      'The \u201cApolitical\u201d category is intended to indicate that the company in question has not made a habit of chiming in regarding social and political issues. Companies are companies, not people, and there are A LOT of us that wish they\u0027d all just shut up and make a good product. The companies in this category got that memo before we ever wrote it as they aren\u0027t trying to use their \u201cplatform\u201d to preach to everyone else.',
  },
  environment: {
    name: 'environment',
    display: 'Environmentally Friendly',
    src: 'https://ubrecoco.sirv.com/Images/categories/environment.webp',
    alt: 'tree inside of green circle',
    description:
      'The \u201cEnvironmentally Friendly\u201d category is intended to indicate that the company in question takes steps to help the environment. While the case for a climate crisis or even just man-made climate change is flimsy at best, full of false claims and bad information, spread by hypocrites, and highly politicized which definitely comes at the cost of credibility, it still isn\u0027t a bad idea to try to be good stewards of this planet. Furthermore, the ecological damage and disasters created many of the billion dollar corporations is of far greater concern, but no one wants to talk about that which is why we should reward the companies that are!',
  },
  foss: {
    name: 'foss',
    display: 'Supports Open Source',
    src: 'https://ubrecoco.sirv.com/Images/categories/foss.webp',
    alt: 'open source logo',
    description:
      'The \u201cSupports Open Source\u201d category is intended to indicate that the company in question is friendly in some way to the open source community. With many software and SaaS companies becoming increasingly hostile through their anti-consumer subscription models, manipulative algorithms, vague TOS, etc, it makes sense to reward the companies that aren\u0027t playing those games and are actually contributing to the open source community.',
  },
  freespeech: {
    name: 'freespeech',
    display: 'Supports Free Speech',
    src: 'https://ubrecoco.sirv.com/Images/categories/freespeech.webp',
    alt: 'a microphone',
    description:
      'The \u201cSupports Free Speech\u201d category is intended to indicate that the company in question is free-speech friendly or actually has made pro-free-speech statements. In a world where those trying to take control of everything simply have their lackeys in the media label everything they don\u0027t want as \u201cmisinformation\u201d while pressuring platforms to censor certain topics, it is refreshing to learn that there are still companies out there that understand the importance of free expression of ideas. The brands/companies seem to understand that without hearing different views from different perspectives, you end up in a 1-sided echo chamber with a hive mind mentality. Also, it is important to note that hearing opposing views can be a catalyst for growth and expansion of your worldview, and that is potentially the most important argument for free speech.',
  },
  madeInUsa: {
    name: 'madeInUsa',
    display: 'Made in the USA',
    src: 'https://ubrecoco.sirv.com/Images/categories/madeInUsa.webp',
    alt: 'made in the USA stamp',
    description:
      'The \u201cMade in the USA\u201d category is intended to indicate that the company in question, well, makes their products in the good old US of A. Sure, you can export manufacturing and customer support jobs to Asian countries to save a buck, but that isn\u0027t helping create jobs in America. The companies that are fine with charging a little more as a way to offset the additional costs of making their products in the USA are helping the people of the country, and that deserves our praise!',
  },
  patriotic: {
    name: 'patriotic',
    display: 'Patriotic',
    src: 'https://ubrecoco.sirv.com/Images/categories/usa.webp',
    alt: 'an American flag',
    description:
      'The \u201cPatriotic\u201d category is intended to indicate that the company in question is pro-USA in some way. Whether it is through social media messaging, donations made to patriotic organizations, creating patriotic merchandise, etc, these companies love the USA and are not afraid to let that show.',
  },
  privacy: {
    name: 'privacy',
    display: 'Respects Privacy',
    src: 'https://ubrecoco.sirv.com/Images/categories/lock.webp',
    alt: 'a padlock',
    description:
      'The \u201cRespects Privacy\u201d category is intended to indicate that the company in question respects the privacy of others when most companies are trying to collect and sell as much of your data as possible. If a brand/company has this category, that means that they don\u0027t fill their sites our apps with 15 different trackers, encrypt data for privacy and security, don\u0027t require your to give a ton of your information to create an account, etc. ',
  },
  repair: {
    name: 'repair',
    display: 'Supports Repair',
    src: 'https://ubrecoco.sirv.com/Images/categories/repair.webp',
    alt: 'a wrench and a hammer',
    description:
      'The \u201cSupports Repair\u201d category is intended to indicate that the company in question understands the concept of ownership better than most. If you buy something, you should not only be able to repair it yourself but should have the freedom to take it to any repair center you wish. Most mega-corporations don\u0027t believe that at all as they intentionally make their products difficult or impossible to repair and force you to use their repair centers which are usually incredibly overpriced, almost as if they\u0027d rather you buy a new product than have you current one fixed. Don\u0027t support those companies! If a company has this category then they either make things easy to repair, supply schematics, sell individual parts, etc and those are the companies would should be supporting.',
  },
};
