import { Organization } from './organizations';

export const goodOrgs: Organization[] = [
  {
    name: 'Front Line COVID-19 Critical Care Alliance (FLCCC)',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/flccc.webp',
    imgHeight: 147,
    description:
      'While the vast majority of medical institutions were pushing fear porn about COVID-19 and were shilling for experimental and dangerous injections from big pharma companies, the FLCCC was actually trying to learn about COVID and give people protocols and guidance for treating the virus. Consisting of highly published, world-renowned physicians and scholars from around the world, the FLCCC has potentially saved thousands of lives through their blogs, videos, and more. Their protocols often contain common sense advice to get enough vitamins and minerals while using things like elderberry, quercetin and other anti-oxidants, and even Ivermectin. Recommending things like vitamin D, zinc, elderberry, resveratrol, etc doesn\u0027t generate money for the medical industry or big pharma, which is why you weren\u0027t hearing other medical institutions recommend these sorts of preventative measures in place. On the other hand, the FLCCC has been researching and advocating for alternate uses of already established and well tested medications that have proven to have some level of success in treating COVID infections. The FLCCC has even appeared before the Senate to discuss early treatment and the benefits of Ivermectin. This is a group that is actually trying to help, and they deserve to be recognized for that!',
    link: 'https://covid19criticalcare.com/',
  },
  {
    name: 'National Association for Gun Rights',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/nagr.webp',
    imgHeight: 310,
    description:
      'The National Association for Gun Rights does what many people think the NRA does, and that is actually fight to protect our right to own and carry firearms. This organization has done work with high-profile court cases relating to gun rights and put pressure on many politicians to protect the second amendment. Their website contains resources to help grassroots gun rights organizations, tracking of current and important gun bills throughout the country, news about laws and court cases pertaining to gun rights, petitions, and more. If you are a law abiding gun owner that is tired of the constant attacking our your constitutional right to own and bear arms, this organization is worth your support!',
    link: 'https://www.nationalgunrights.org/',
  },
  {
    name: 'The Repair Association',
    logo: 'https://ubrecoco.sirv.com/Images/organizations/repairorg.webp',
    imgHeight: 113,
    description:
      'The Repair Association believe that if you own something, you should be able to repair it yourself or choose who you want to repair it. Pretty simple and agreeable, huh? The idea of ownership is under attack. Companies like Apple try to make everything unrepairable or only repairable at their locations. BMW will sell you a car with the hardware for heated seats, but they won\u0027t work unless you pay a monthly subscription. Many auto manufacturers are trying to make it such that they can require you to have all repairs done at the dealership with a huge mark-up which directly profits them. We need a coordinated effort to push back onto these things, and the Repair Association of one of a handful of organizations trying to fight that fight!',
    link: 'https://www.repair.org/',
  },
];
