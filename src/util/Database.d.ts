export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[];

export interface Database {
  public: {
    Tables: {
      alternatives: {
        Row: {
          created_at: string | null;
          categories: string[] | null;
          logo: string | null;
          name: string | null;
          reasons: Json | null;
          tags: Json | null;
          uuid: string;
        };
        Insert: {
          created_at?: string | null;
          categories?: string[] | null;
          logo?: string | null;
          name?: string | null;
          reasons?: Json | null;
          tags?: Json | null;
          uuid?: string;
        };
        Update: {
          created_at?: string | null;
          categories?: string[] | null;
          logo?: string | null;
          name?: string | null;
          reasons?: Json | null;
          tags?: Json | null;
          uuid?: string;
        };
      };
      companies: {
        Row: {
          alternatives: Json | null;
          created_at: string | null;
          categories: Json | null;
          logo: string | null;
          name: string | null;
          reasons: Json | null;
          subsidaries: Json | null;
          tags: Json | null;
          uuid: string;
        };
        Insert: {
          alternatives?: Json | null;
          created_at?: string | null;
          categories?: Json | null;
          logo?: string | null;
          name?: string | null;
          reasons?: Json | null;
          subsidaries?: Json | null;
          tags?: Json | null;
          uuid: string;
        };
        Update: {
          alternatives?: Json | null;
          created_at?: string | null;
          categories?: Json | null;
          logo?: string | null;
          name?: string | null;
          reasons?: Json | null;
          subsidaries?: Json | null;
          tags?: Json | null;
          uuid?: string;
        };
      };
    };
    Views: {
      [_ in never]: never;
    };
    Functions: {
      [_ in never]: never;
    };
    Enums: {
      [_ in never]: never;
    };
    CompositeTypes: {
      [_ in never]: never;
    };
  };
}
