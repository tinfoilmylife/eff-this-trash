import OrganizationItem from '@/components/OrganizationItem';
import Header from '@/components/site/Header';
import Wrapper from '@/components/site/Wrapper';
import { goodOrgs } from '@/util/goodOrgs';
import { Organization } from '@/util/organizations';
import { Metadata } from 'next';
import Image from 'next/image';

export const metadata: Metadata = {
  title: 'Good Organizations',
  description: 'Good organizations fighting the good fight.',
  keywords: ['right tot repair', 'wokeness', 'evil companies'],
  openGraph: {
    title: 'Eff This Trash | Good Organizations',
    description: 'Good organizations fighting the good fight.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/goodorgs',
    siteName: 'Eff This Trash | Good Organizations',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | Good Organizations',
    description: 'Good organizations fighting the good fight.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function GoodOrgs() {
  const textClasses = 'text-gray-50 leading-7 text-xl initial-letter-red';
  return (
    <>
      <Header active="/info" activeChild={'Good Organizations'} />
      <Wrapper>
        <div className="w-full flex-column items-center" id="terms">
          <h1 className="text-red-500 text-center">Good Organizations</h1>
          <div className="w-full flex flex-col items-center justify-center mt-4 px-4">
            <Image
              src="https://ubrecoco.sirv.com/Images/general/good%20(2).webp"
              width={1280}
              height={802}
              alt="man in a suit giving a thumb up"
              priority
              className="max-w-2xl w-full"
            />
            <p className={`${textClasses} my-8 max-w-4xl`}>
              There are a handful or good organizations actually attempting to
              provide information, services, and more that benefit everyone as a
              whole and that aren&apos;t acting in a politically or
              ideologically motivated manner. We should all appreciate these
              organizations, as it seems to be becoming less common for these
              types of organizations to exist. Below, you will find a short list
              of a handful of organizations worth checking out.
            </p>
          </div>
          <div className="">
            {goodOrgs.map((org: Organization) => {
              return <OrganizationItem org={org} key={org.name} />;
            })}
          </div>
        </div>
      </Wrapper>
    </>
  );
}
