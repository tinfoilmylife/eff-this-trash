import Table from '@/components/Table';
import Header from '@/components/site/Header';
import Wrapper from '@/components/site/Wrapper';
import { Metadata } from 'next';
import companies from '../../data/goodCompanies.json';
import Image from 'next/image';
import { goodCategories } from '@/util/constants';

export const metadata: Metadata = {
  title: 'Good Brands',
  description:
    'Support brands and companies that do not engage in politics and/or social discussions as well as the ones that do but support liberty, freedom, etc.',
  keywords: ['liberty', 'freedom', 'values'],
  openGraph: {
    title: 'Eff This Trash | Good Brands',
    description:
      'Support brands and companies that do not engage in politics and/or social discussions as well as the ones that do but support liberty, freedom, etc.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/goodbrands',
    siteName: 'Eff This Trash | Good Brands',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | Good Brands',
    description:
      'Support brands and companies that do not engage in politics and/or social discussions as well as the ones that do but support liberty, freedom, etc.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

const header = [
  {
    text: 'Company',
    sortable: true,
  },
  {
    text: 'Site',
    sortable: false,
  },
  {
    text: 'Categories',
    sortable: false,
  },
  {
    text: 'Reasons',
    sortable: false,
  },

  {
    text: 'Alternative to',
    sortable: false,
  },
  {
    text: 'Brands',
    sortable: false,
  },
];
const columns = [
  {
    key: 'name',
    format: 'returnCompany',
  },
  {
    key: 'website',
    format: 'returnSiteLink',
  },
  {
    key: 'categories',
    format: 'returnCategories',
  },
  {
    key: 'reasons',
    format: 'returnMain',
  },
  {
    key: 'alternativeTo',
    format: 'returnString',
  },
  {
    key: 'subsidaries',
    format: 'returnButtonDialog',
  },
];

const GoodBrands: React.FC = () => {
  return (
    <>
      <div className="flex-col" style={{ flexGrow: 1 }}>
        <Header active={'/goodbrands'} />
        <Wrapper>
          <h1 className="py-2 text-red-500 text-center">Good brands</h1>
          <div className="mb-8 flex flex-col items-center w-full">
            <Image
              src="https://ubrecoco.sirv.com/Images/general/good%20(1).webp"
              alt="coffee beans with thumbs up icon on top"
              className="rounded-md shadow-md"
              width={610}
              height={406}
              unoptimized
              loading="lazy"
            />
            <p className="mt-8 max-w-4xl text-lg text-center initial-letter-red">
              Here is a list of brands and companies that, according to what I
              can find online, are not playing the stupid games played by the
              companies on the &ldquo;Garbage brands&rdquo; page. Whether they
              are patriotic, conservative or at least apolitical, made in
              America, pro right-to-repair, respect your privacy, etc., these
              brands are not actively trying to destroy our culture and
              participate in the globalists&apos; oppression olympics. They just
              make a good product!
            </p>
          </div>
          {companies && (
            <Table
              // @ts-ignore dynamic data
              data={companies || []}
              header={header}
              details={{ defaultSortColumn: 'name', defaultSortDir: 'asc' }}
              // @ts-ignore later
              columns={columns}
              categoriesList={goodCategories}
            />
          )}
        </Wrapper>
      </div>
    </>
  );
};

export default GoodBrands;
