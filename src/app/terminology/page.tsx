import Header from '@/components/site/Header';
import Term from '@/components/Term';
import Wrapper from '@/components/site/Wrapper';
import { terms } from '@/util/terminology';
import { Metadata } from 'next';
import Image from 'next/image';

export const metadata: Metadata = {
  title: 'Terminology',
  description: 'Know the terminology used in the culture war.',
  keywords: ['terminology', 'culture war', 'LGBT', 'ESG', 'DEI', 'DRM'],
  openGraph: {
    title: 'Eff This Trash | Terminology',
    description: 'Know the terminology used in the culture war.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/terminology',
    siteName: 'Eff This Trash | Terminology',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | Terminology',
    description: 'Know the terminology used in the culture war.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function Terminology() {
  const textClasses = 'text-gray-50 leading-7 text-xl';
  return (
    <>
      <Header active="/info" activeChild={'Terminology'} />
      <Wrapper>
        <div className="w-full flex-column items-center" id="terms">
          <h1 className="text-red-500 text-center">Terminology</h1>
          <div className="w-full flex flex-col justify-center items-center mt-4 px-4">
            <Image
              src="https://ubrecoco.sirv.com/Images/general/terms.webp"
              width={1280}
              height={853}
              priority
              className="max-w-2xl w-full"
              alt="dictionary pages full of text"
              unoptimized
            />
            <p className={`${textClasses} my-8 initial-letter-red max-w-4xl`}>
              It helps to know and understand the words and phrases used in the
              culture war, right-to-repair space, and more. We can take that
              knowledge and use it to better understand the discussions going on
              around us, then push back against ideas that are just plain
              terrible. Additionally, those pushing “the current thing” are
              redefining a lot of words. I&apos;m sorry, but someone&apos;s
              feelings about what a word should mean now matters not when that
              word has been around for thousands of years and meant something
              different. Here is where you will find a detailed and fairly
              opinionated overview of some of these terms and phrases.
            </p>
          </div>
          <div>
            {terms.map((term) => (
              <Term
                word={term.word as string}
                description={term.description}
                phonetics={term.phonetics}
                type={term.type}
                key={term.word}
              />
            ))}
          </div>
        </div>
      </Wrapper>
    </>
  );
}
