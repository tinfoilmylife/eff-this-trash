import Header from '@/components/site/Header';
import Table from '@/components/Table';
import Wrapper from '@/components/site/Wrapper';
import companies from '../../data/companies.json';
import { Metadata } from 'next';
import Image from 'next/image';
import { categoriesList } from '@/util/constants';

export const metadata: Metadata = {
  title: 'Garbage Brands',
  description:
    'Avoid giving money to companies that are woke, anti-consumer, politically biased, and worse.',
  keywords: ['wokeness', 'leftist', 'biased', 'evil', 'greed'],
  openGraph: {
    title: 'Eff This Trash | Garbage Brands',
    description:
      'Avoid giving money to companies that are woke, anti-consumer, politically biased, and worse.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/garbagebrands',
    siteName: 'Eff This Trash | Garbage Brands',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | Garbage Brands',
    description:
      'Avoid giving money to companies that are woke, anti-consumer, politically biased, and worse.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

const GarbageBrands = () => {
  const header = [
    {
      text: 'Company',
      sortable: true,
    },
    {
      text: 'Categories',
      sortable: false,
    },
    {
      text: 'Score',
      sortable: true,
    },
    {
      text: 'Reasons',
      sortable: false,
    },

    {
      text: 'Alternatives',
      sortable: false,
    },
    {
      text: 'Brands',
      sortable: false,
    },
  ];
  const columns = [
    {
      key: 'name',
      format: 'returnCompany',
    },
    {
      key: 'categories',
      format: 'returnCategories',
    },
    {
      key: 'score',
      format: 'formatScoreBad',
    },
    {
      key: 'reasons',
      format: 'returnMain',
    },
    {
      key: 'alternatives',
      format: 'returnString',
    },
    {
      key: 'subsidaries',
      format: 'returnButtonDialog',
    },
  ];
  return (
    <>
      <div className="flex-col" style={{ flexGrow: 1 }}>
        <Header active={'/garbagebrands'} />
        <Wrapper>
          <h1 className="py-2 text-red-500 text-center">Garbage brands</h1>
          <div className="mb-8 flex flex-col items-center w-full">
            <Image
              src="https://ubrecoco.sirv.com/Images/general/bad%20(1).webp"
              alt="trash can with wads of paper in and around it"
              className="rounded-md shadow-md"
              width={610}
              height={406}
              unoptimized
              loading="lazy"
            />
            <p className="mt-8 max-w-4xl text-lg text-center initial-letter-red">
              This page is not here telling you to boycott every company on this
              list. Instead, this is here to make you aware of some things that
              these companies have done, so you are better informed to make a
              decision regarding whether you want to support these companies
              anymore. Ultimately, each person should decide which issues matter
              the most to them and which actions they wish to take to make a
              change.
            </p>
          </div>
          {companies && (
            <Table
              // @ts-ignore dynamic data
              data={companies || []}
              header={header}
              details={{ defaultSortColumn: 'name', defaultSortDir: 'asc' }}
              // @ts-ignore later
              columns={columns}
              categoriesList={categoriesList}
            />
          )}
        </Wrapper>
      </div>
    </>
  );
};

export default GarbageBrands;
