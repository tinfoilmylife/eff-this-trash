import Header from '@/components/site/Header';
import Wrapper from '@/components/site/Wrapper';
import { Metadata } from 'next';
import effThisTrashLight from '../../../public/effThisTrash-light.png';
import Image from 'next/image';

export const metadata: Metadata = {
  title: 'About',
  description: 'A little about EffThisTrash.com',
  keywords: ['effthistrash', 'Tinfoil Guy'],
  openGraph: {
    title: 'Eff This Trash | About',
    description: 'A little about EffThisTrash.com',
    images: '/effThis.png',
    url: 'https://effthistrash.com/about',
    siteName: 'Eff This Trash | About',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | About',
    description: 'A little about EffThisTrash.com',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function About() {
  const textClasses = 'text-gray-50 leading-7 text-xl initial-letter';
  return (
    <>
      <Header active={'/about'} />
      <Wrapper>
        <div className="flex justify-center">
          <Image
            src={effThisTrashLight}
            alt="trash can illustration and website name"
            className="w-full max-w-3xl"
          />
        </div>
        <h1 className="py-2 text-red-500">About this site</h1>
        <article className="p-2 md:p-8">
          <h2 className="py-2 text-red-500">The CYA section</h2>
          <ul className={`${textClasses} list-disc ml-3 md:ml-0`}>
            <li>
              The ideas on this site are the opinion of Tinfoil Guy who will
              remain anonymous because eff you, that is why! Nothing said on
              this page is intended to be defamitory as everything posted about
              the various organizations and companies mentioned on this site are
              thought to be true statements with evidence to support those
              statements. If the web has given Tinfoil Guy
              &ldquo;misinformation&rdquo; which was spread on this site,
              that&apos;s the fault of the individual actor that initially put
              that bad information out into the world. There is a strong effort
              being made to avoid the spreading of any bad information, but
              nobody is perfect. That said, if I&apos;ve posted something
              factually incorrect (and facts don&apos;t give AF about your
              delicate little feelings) then Tinfoil Guy is truly sorry and will
              try to do better in the future.
            </li>
            <li>
              Tinfoil Guy is very much a &ldquo;live and let live&rdquo; and
              &ldquo;to each their own&rdquo; kind of person. That said,
              understand that there is no hatred of gay, trans, overweight, etc
              peoeple. If you want to be one of these people, that is 100% your
              right. The issues discussed on this site are more about the
              endless shoving of these ideas down our throats and inappropriate
              exposure and indoctrination of children with these ideas. If a
              company wants to give money to some LGBT cause, that&apos;s there
              choice and no issue is taken. If they want to sell LGBT books to
              small kids and have a drag queen dressed in an overtly sexualized
              manner read these books to kids, there is the effing problem.
            </li>
          </ul>
          <article className="mt-4">
            <h2 className="py-2 text-red-500">Why build this site?</h2>
            <p className={textClasses}>
              Why not build this site? Every generation that has ever lived has
              thought they were living in the worst of times or the end times.
              The difference this time is that we&apos;ve never before had such
              a coordinated and well-connected effort to essentially cull the
              human race and enslave those that remain. Freedom is dying a slow
              death, and there is no shortage of corporations helping usher in
              this reality as they will do whatever is necessary to remain in
              the central banks&apos; good graces, even if it means becoming
              morally bankrupt in the process. Greed is driving every major
              decision now. I thought that making a website to help expose some
              of the most egregious purveyors of this agenda, intentional or
              not, would be yet another extremely small effort to spread
              awareness and push back against these efforts. One of the biggest
              challenges in awakening people to the harsh and dystopian
              realities of the world is that things have become so insane and
              outlandish that it is difficult to get the uninitiated to give
              actual consideration to these truths. I thought that creating a
              website documenting the malicious behavior of the corporations
              helping create these truths was the least I could do. There have
              been some incredibly successful boycotts lately that have had a
              real impact, and this means that people are finally becoming fed
              up enough to do something. Let&apos;s throw gasoline on that fire
              and really shake things up! You and I possess more power to create
              change than the folks at the top want us to realize we have. If we
              stick to our guns, we can eventually bring some of them to their
              knees while sending a message to all of them that we will no
              longer tolerate their BS! If a company sees another politically
              biased, ultra-woke, DEI and ESG peddling company suffer massive
              losses because of a successful boycott, it would surely make them
              think twice before going down that same path. This site aims to be
              another beacon of information and hope for those trying to fight
              back against the evil and insane globalist movement taking a grip
              on a world before we live in complete tyranny.
            </p>
          </article>
          <article className="mt-4">
            <h2 className="py-2 text-red-500">Who is Tinfoil Guy?</h2>
            <div className="flex justify-center mb-8">
              <Image
                src={'https://ubrecoco.sirv.com/Images/assets/tinfoil.webp'}
                alt="guy wearing a tinfoil hat"
                className="w-full max-w-xs"
                width={389}
                height={390}
                fill={false}
                unoptimized
              />
            </div>
            <p className={textClasses}>
              I&apos;m a middle-aged Gen-Xer that works as a software engineer
              and constantly makes stupid projects like this one as an excuse to
              use new technologies that I&apos;ve not used before. I value
              privacy, freedom, common sense, honesty, accountability, and
              integrity. There is a real shortage of those things in the modern
              world, and that is a huge motivating factor as to why I felt the
              need to build this specific site. I&apos;m also a father having to
              navigate raising a kid in this cesspool while attempting to keep
              my child&apos;s mind pure and open, and that is proving
              increasingly difficult to do. That said, I long for cultural
              change and I believe that we have the power to create change if
              enough of us choose to exercise that power. I sit around pondering
              how I could potentially help in steering the ship back in the
              correct direction, as I want my child to grow up in a world that
              more resembles the world in which I grew up. The 80s and 90s
              weren&apos;t perfect, but at least there was less evil, more
              emphasis and critical thinking and individuality, and less
              division in those times. As someone who reached “adulthood” in the
              90s, I often look back at my wordly experiences and remember a
              time when racism seemed close to being a thing of the past, where
              people didn&apos;t let the differences in their beliefs foster
              hostility toward each other, where politics were not treated as
              team sports and people knew about the ideas and plans of a
              candidate for which they voted, and where we just generally got
              along and strived to be the best version of ourselves. I now see a
              world filled with ignorance, narcissism, degeneracy, hatred, and
              tribalism, and it is all creating an ever accelerating downward
              spiral toward a reality that will be good for no one except a very
              few people at the top of the food chain. I don&apos;t want to live
              in that world, and I hope you don&apos;t either. Eff the current
              thing, eff these clown shoe companies and organizations, and eff
              every useful idiot unwittingly helping usher in the new world
              order!
            </p>
          </article>
          <article className="mt-4">
            <h2 className="py-2 text-red-500">What can I do to help?</h2>
            <p className={textClasses}>
              Helping is easy. Just push back against the companies that are
              helping usher in this dystopian future of which we do not approve.
              Decide which issues matter most to you and avoid giving your
              hard-earned money to the companies that push those agendas. Also,
              share this site with your friends, even the ones with their heads
              in the sand that don&apos;t realize what is really going on in the
              world. There will eventually ways to donate to help pay for this
              site to stay up and maybe even help pay for my time to develop and
              maintain it, but I omitted adding that for the initial launch as I
              wanted to get this site out into the world as quickly as possible.
            </p>
          </article>
        </article>
      </Wrapper>
    </>
  );
}
