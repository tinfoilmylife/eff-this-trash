import Header from '@/components/site/Header';
import OrganizationItem from '@/components/OrganizationItem';
import Wrapper from '@/components/site/Wrapper';
import { Organization, organizations } from '@/util/organizations';
import { Metadata } from 'next';
import Image from 'next/image';

export const metadata: Metadata = {
  title: 'Evil Organizations',
  description: 'Evil organizations that should be avoided.',
  keywords: ['Blackrock', 'control', 'greed', 'new world order'],
  openGraph: {
    title: 'Eff This Trash | Evil Organizations',
    description: 'Evil organizations that should be avoided.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/evilorgs',
    siteName: 'Eff This Trash | Evil Organizations',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | Evil Organizations',
    description: 'Evil organizations that should be avoided.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function EvilOrgs() {
  const textClasses = 'text-gray-50 leading-7 text-xl initial-letter-red';
  return (
    <>
      <Header active="/info" activeChild={'Evil Organizations'} />
      <Wrapper>
        <div className="w-full flex-column items-center">
          <h1 className="text-red-500 mb-8 text-center">Evil Organizations</h1>
          <div className="w-full flex flex-col items-center justify-center mt-4 px-4">
            <Image
              src="https://ubrecoco.sirv.com/Images/general/evil.webp"
              width={1280}
              height={853}
              alt="some skulls stacked together"
              priority
              className="max-w-2xl w-full"
            />
            <p className={`${textClasses} my-8 max-w-4xl`}>
              There are a handful of organizations and companies in the world
              that are working together for power, money, and control. Their
              actions are harmful to society as a whole and the world they are
              working together to build is one where we all have fewer rights,
              freedoms, power, and property. It is well worth knowing about
              these organizations and companies as they will not only be
              mentioned on other pages of this site, but awareness of these
              entities and their actions will empower us all to form a more
              complete view of the world around us while giving us the needed
              knowledge to push back more effectively. If a company has a strong
              affiliation with any of the entities listed here, it is probably a
              good idea to avoid supporting them. This list is full of opinions
              that have been formed based on a lot of evidence, so feel free to
              do your own research if you want to learn more about each of these
              organizations.
            </p>
            <p className={`${textClasses} my-8 max-w-4xl`}>
              There is a bit of opinion and humor sprinkled into the below
              sections, but as with everything on this website, an attempt was
              made to ensure that everything said here is as fact based as
              possible and you can easily &ldquo;do your own research&rdquo; and
              find links that support all of it…as long as you are not using
              Google Search which has also become a tool of the authoritarians
              as they censor and derank search results that go against the
              accepted narrative.
            </p>
          </div>
          <div>
            {organizations.map((org: Organization) => {
              return <OrganizationItem org={org} key={org.name} />;
            })}
          </div>
        </div>
      </Wrapper>
    </>
  );
}
