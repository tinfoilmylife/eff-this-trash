import Features from '@/components/Features/Features';
import Header from '@/components/site/Header';
import Wrapper from '@/components/site/Wrapper';
import Image from 'next/image';
import { theme } from '../../tailwind.config';
import { Metadata } from 'next';
import effThisTrashLight from '../../public/effThisTrash-light.png';

export const metadata: Metadata = {
  title: 'Eff This Trash',
  description:
    "It's time to make a change with our wallets and stop giving money to garbage companies.",
  keywords: ['boycotts', 'shopping', 'activism'],
  openGraph: {
    title: 'Eff This Trash',
    description:
      "It's time to make a change with our wallets and stop giving money to garbage companies.",
    images: '/effThis.png',
    url: 'https://effthistrash.com',
    siteName: 'Eff This Trash',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash',
    description:
      "It's time to make a change with our wallets and stop giving money to garbage companies.",
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function Home() {
  return (
    <>
      <Header active={'/'} />
      <Wrapper>
        <div className="route-container">
          <div className="flex flex-row items-center justify-center w-full mt-16 mb-4 px-8">
            <Image
              src={effThisTrashLight}
              alt='trash can illustration and "eff this trash" text'
              className="w-full md:w-9/12 lg:w-1/2 xl:w-2/5 2xl:w-auto"
              priority
            />
          </div>
          <div className="w-full flex flex-row justify-center mb-16">
            <div
              className="w-full md:w-9/12 lg:w-1/2 xl:w-2/5 2xl:w-auto text-center font-bold text-xl sm:text-2xl md:text-4xl text-red-500"
              style={{
                // @ts-ignore
                textShadow: `${theme.extend.colors.gray['50']} 1px 0 1px`,
              }}
            >
              It&apos;s time to make a change with our wallets and stop giving
              money to garbage companies.
            </div>
          </div>
          <div className="flex flex-row w-full justify-center">
            <Features />
          </div>
        </div>
      </Wrapper>
    </>
  );
}
