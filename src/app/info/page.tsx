import CategoryItem from '@/components/CategoryItem';
import Header from '@/components/site/Header';
import Wrapper from '@/components/site/Wrapper';
import { categoriesList, goodCategories } from '@/util/constants';
import { CategoryT, GoodCategoryT } from '@/util/types';
import { Metadata } from 'next';
import Image from 'next/image';

export const metadata: Metadata = {
  title: 'General Info',
  description:
    'Arm yourself with information about these companies to help ensure you make better decisions regarding who to support.',
  keywords: ['right to repair', 'wokeness', 'evil companies'],
  openGraph: {
    title: 'Eff This Trash | General Info',
    description:
      'Arm yourself with information about these companies to help ensure you make better decisions regarding who to support.',
    images: '/effThis.png',
    url: 'https://effthistrash.com/info',
    siteName: 'Eff This Trash | General Info',
    locale: 'en_US',
    type: 'website',
  },
  twitter: {
    title: 'Eff This Trash | General Info',
    description:
      'Arm yourself with information about these companies to help ensure you make better decisions regarding who to support.',
    card: 'summary_large_image',
    images: ['/effThisTrash-light.png'],
  },
};

export default function Info() {
  const textClasses = 'text-gray-50 leading-7 text-xl initial-letter-red';

  return (
    <>
      <Header active={'/info'} activeChild="General" />
      <Wrapper>
        <div className="route-container">
          <h1 className="text-red-500 mb-8 text-center">Information</h1>
          <div className="w-full flex-column items-center">
            <div className="flex justify-center mt-4 px-4">
              <Image
                src={'https://ubrecoco.sirv.com/Images/general/money.webp'}
                alt="cash and Bitcoin"
                className="max-w-2xl w-full"
                width={1920}
                height={1440}
                unoptimized
              />
            </div>
            <div className="w-full flex justify-center">
              <p
                className={`${textClasses} mt-4 max-w-4xl text-center initial-letter-red`}
              >
                Maybe you are pro-right-to-repair and want to avoid giving your
                money to a very anti-consumer company? Maybe you are sick of the
                degeneracy taking hold of our culture and the companies that
                shove these ideas down our throats? Regardless of why you are
                here and what issue about which you are most concerned, you can
                arm yourself with knowledge and start pushing back with your
                wallet. Money seems to be the universal language of business and
                carefully choosing which businesses you give your money to is
                the easiest way we can make a statement.
              </p>
            </div>
            <section
              className="w-full flex-column items-center mt-16"
              id="categories"
            >
              <h2 className="text-red-500 mb-8 flex">
                Categories for the bad brands/companies
              </h2>
              <p className={`${textClasses}`}>
                On the Garbage Brands page, you will notice a column in the
                table of companies labelled “Categories”. This column is
                intended to provide a quick reference to the specific offenses
                that each company has committed. Below, you will find a brief
                overview of how each of these categories is defined. None of
                this is meant to be hateful, but aims to be critical of ideas
                that are commonplace in society today and the companies that
                insist on perpetuating these ideas.
              </p>
              <div className="">
                {Object.keys(categoriesList).map((cat: string) => {
                  return (
                    <CategoryItem
                      category={categoriesList[cat as CategoryT]}
                      key={cat}
                    />
                  );
                })}
              </div>
            </section>
            <section
              className="w-full flex-column items-center mt-16"
              id="good-categories"
            >
              <h2 className="text-red-500 mb-8 flex">
                Categories for the GOOD brands/companies
              </h2>
              <p className={`${textClasses}`}>
                On the Good Brands page, you will notice a column in the table
                of companies labelled “Categories”. This column is intended to
                provide a quick reference to the specific reasons for which we
                SHOULD be supporting these companies and brands. Let’s give our
                hard earn cash to companies that love their customers and this
                country and do so with integrity and principles.
              </p>
              <div className="">
                {Object.keys(goodCategories).map((cat: string) => {
                  return (
                    <CategoryItem
                      category={goodCategories[cat as GoodCategoryT]}
                      key={cat}
                    />
                  );
                })}
              </div>
            </section>
          </div>
        </div>
      </Wrapper>
    </>
  );
}
