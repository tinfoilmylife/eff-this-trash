import { MetadataRoute } from 'next';

export default function sitemap(): MetadataRoute.Sitemap {
  return [
    {
      url: 'https://effthistrash.com',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/about',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/privacypolicy',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/info',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/terminology',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/evilorgs',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/goodorgs',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/garbagebrands',
      lastModified: new Date(),
    },
    {
      url: 'https://effthistrash.com/goodbrands',
      lastModified: new Date(),
    },
  ];
}
