'use client';

interface FilterChipProps {
  label: string;
  name: string;
  selected: boolean;
  toggleChip: (label: string) => void;
  classes?: string;
}

export default function FilterChip({
  label,
  name,
  selected,
  toggleChip,
  classes,
}: FilterChipProps) {
  return (
    <button
      className={`
        rounded-full py-1 px-2 sm:px-3 text-sm sm:text-base ${classes || ''} ${
        selected
          ? ' bg-red-500 hover:bg-red-600 text-gray-50 font-bold'
          : ' bg-teal-600 hover:bg-teal-700 text-gray-100 italic'
      }`}
      onClick={() => toggleChip(name)}
    >
      {label}
    </button>
  );
}
