'use client';

import { BadFilterObj, FiltersItem } from '@/util/types.js';
import FilterChip from './FilterChip';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { JointCatObj } from '@/util/constants';
import { Button } from '../basic/Button';
import Card from '../basic/Card';
import { BsCheck2All } from 'react-icons/bs';
import { HiBan } from 'react-icons/hi';

interface FiltersProps {
  onChipClick: (fils: FiltersItem[]) => void;
  allCats: JointCatObj;
}

export default function Filters({ onChipClick, allCats }: FiltersProps) {
  const [currentFilters, setCurrentFilters] = useState<FiltersItem[]>();
  const chips = useMemo((): FiltersItem[] => {
    // @ts-ignore
    return Object.keys(allCats)
      .map((filter: string) => {
        // @ts-ignore
        const thisFilter = allCats[filter];
        if (thisFilter) {
          return {
            display: thisFilter.display,
            name: thisFilter.name,
            selected: true,
          };
        }
        return null;
      })
      .filter((i) => !!i);
  }, [allCats]);

  const chipClick = useCallback(
    (chip: string) => {
      const copyAll = [...(currentFilters || [])];
      const filterNames = copyAll.map((c: FiltersItem) => c.name);
      const copyIndex = filterNames.indexOf(chip);
      if (copyIndex >= 0 && currentFilters) {
        copyAll[copyIndex].selected = !currentFilters[copyIndex].selected;
        setCurrentFilters(copyAll);
        onChipClick(copyAll);
      }
    },
    [currentFilters, onChipClick]
  );

  const toggleAllCats = (which: boolean) => {
    const copyAll = [...(currentFilters || [])];
    const selected = copyAll.map((f: FiltersItem) => {
      f.selected = which;
      return f;
    });
    setCurrentFilters(selected);
    onChipClick(selected);
  };

  useEffect(() => {
    setCurrentFilters(chips);
  }, [chips]);

  return (
    <Card corners="md" bgClass="bg-gray-900 mt-4 pt-3">
      <div className="mb-2">
        <label className="text-xl text-bold">Filter by category</label>
        <p className="italic">
          The reason given for each brand/company to appear in the table below
          have been sorted into categories. You can click each individual
          category chip below to toggle the visibility of results with the
          corresponding category.
        </p>
      </div>
      <div className="w-full">
        <Button
          variant="primary"
          onButtonClick={() => toggleAllCats(true)}
          classes="py-2 px-2 mr-2 my-1"
        >
          <div className="flex items-center">
            <BsCheck2All className="mr-2" />
            Select all categories
          </div>
        </Button>
        <Button
          variant="warning"
          onButtonClick={() => toggleAllCats(false)}
          classes="py-2 px-2 my-1"
        >
          <div className="flex items-center">
            <HiBan className="mr-2" />
            Deselect all categories
          </div>
        </Button>
        <button></button>
      </div>
      <div className="flex flex-wrap mt-2 w-full">
        {currentFilters?.map((filter: FiltersItem, index: number) => {
          return (
            <FilterChip
              label={filter.display}
              name={filter.name}
              selected={filter.selected}
              toggleChip={chipClick}
              classes="mr-2 mt-2"
              // classes={`my-2 ${index !== 0 ? ' ml-4' : ''}`}
              key={`${filter.name}-filter-chip`}
            />
          );
        })}
      </div>
    </Card>
  );
}
