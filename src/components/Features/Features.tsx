import { HiTrash, HiStar, HiInformationCircle } from 'react-icons/hi';
import { ReactNode } from 'react';
import Card from '../basic/Card';
import { FeatureWrap } from './FeatureWrap';
import { FeatureTextSection } from './FeatureTextSection';
import { FeatureCta } from './FeatureCta';
import Image from 'next/image';

interface FeatureItem {
  // @ts-ignore no-explicit-any
  icon: any;
  title: string;
  description: string;
  link: string;
  linkText: string;
  image: string;
  altText: string;
}

export default function Features() {
  const featureItems = [
    {
      icon: <HiTrash className="mr-3 w-10 h-10" />,
      title: 'Boycott the bad',
      description:
        'There is no shortage of decent brands and companies out there from which we can purchase items and goods so it makes no sense to give our hard earned money to companies that hate us. Let\u0027s fight with our wallets and maybe we can steer our culture back into a healthier direction!',
      link: '/garbagebrands',
      linkText: 'Take out the trash!',
      image: 'https://ubrecoco.sirv.com/Images/general/bad%20(1).webp',
      altText: 'trash can with wads of paper in and around it',
    },
    {
      icon: <HiStar className="mr-3 w-10 h-10" />,
      title: 'Support the good',
      description:
        'We need to be actively seeking out better brands and companies and giving them our money. Let\u0027s reward the apolitical and liberty supporting companies so we can reward proper behavior while avoiding the idea of enriching the garbage brands and companies.',
      link: '/goodbrands',
      linkText: 'Companies to support',
      image: 'https://ubrecoco.sirv.com/Images/general/good%20(1).webp',
      altText: 'coffee beans with thumbs up icon on top',
    },
    {
      icon: <HiInformationCircle className="mr-3 w-10 h-10" />,
      title: 'Armed with info',
      description:
        'There is a lot of information to know. We can arm ourselves with information so we can decide what issues matter most to us so we can all push back in our own way. Learn about what these brands and companies are doing so you can decide which issues matter most to you.',
      link: '/info',
      linkText: 'I want to learn more',
      image: 'https://ubrecoco.sirv.com/Images/general/library%20(1).webp',
      altText: 'a library aisle full of books',
    },
  ];

  // @ts-ignore no-explicit-any
  const titleComp = (title: string, icon: any) => {
    return (
      <div className="flex justify-center items-center text-red-500">
        <div className="mb-5">{icon}</div>
        <h2 className="mb-4 text-4xl">{title}</h2>
      </div>
    );
  };

  const TextWrap = ({ children }: { children: ReactNode }) => {
    return <div className="text-center w-full mb-2 h-full">{children}</div>;
  };

  const leftImageSection = (item: FeatureItem) => {
    return (
      <FeatureWrap>
        <TextWrap>
          {titleComp(item.title, item.icon)}
          {/* {textSection(item)} */}
          <FeatureTextSection
            text={item.description}
            link={{ link: item.link, linkText: item.linkText }}
          />
        </TextWrap>
        <div>
          <Image
            src={item.image}
            alt={item.altText}
            className="rounded-md shadow-md"
            width={610}
            height={406}
            unoptimized
            loading="lazy"
          />
        </div>
      </FeatureWrap>
    );
  };

  const rightImageSection = (item: FeatureItem) => {
    return (
      <FeatureWrap>
        <div>
          <Image
            src={item.image}
            alt={item.altText}
            className="rounded-md shadow-md"
            width={610}
            height={406}
            unoptimized
            loading="lazy"
          />
        </div>
        <TextWrap>
          {titleComp(item.title, item.icon)}
          {/* {textSection(item)} */}
          <FeatureTextSection
            text={item.description}
            link={{ link: item.link, linkText: item.linkText }}
          />
        </TextWrap>
      </FeatureWrap>
    );
  };

  const mobileCard = (item: FeatureItem) => {
    return (
      <Card corners="md" bgClass="bg-gray-950">
        <div className="flex-colum items-center justify-center">
          {titleComp(item.title, item.icon)}
          <div className="flex justify-center">
            <Image
              src={item.image}
              alt={item.altText}
              className="rounded-md shadow-md"
              width={610}
              height={406}
              unoptimized
              loading="lazy"
            />
          </div>
          <div className="text-gray-50 text-lg sm:text-2xl leading-9 text-center">
            {item.description}
            <FeatureCta item={{ link: item.link, linkText: item.linkText }} />
          </div>
        </div>
      </Card>
    );
  };

  return (
    <div className="flex-column w-full xl:w-9/12 mt-8">
      {featureItems.map((item, index) => {
        return (
          <div className="my-8 lg:my-20" key={item.title}>
            <div className="hidden lg:block">
              {index % 2 === 0
                ? leftImageSection(item)
                : rightImageSection(item)}
            </div>
            <div className="lg:hidden">{mobileCard(item)}</div>
          </div>
        );
      })}
    </div>
  );
}
