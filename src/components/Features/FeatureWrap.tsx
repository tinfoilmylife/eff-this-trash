import { ReactNode } from 'react';

export const FeatureWrap = ({ children }: { children: ReactNode }) => {
  return (
    <div className="w-full lg:grid lg:grid-cols-2 gap-8 content-center">
      {children}
    </div>
  );
};
