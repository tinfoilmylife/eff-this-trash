import { FeatureTextSectionLink } from '@/util/types';
import { FeatureCta } from './FeatureCta';

interface FeatureTextSection {
  text: string;
  link?: FeatureTextSectionLink;
}

export const FeatureTextSection = ({ text, link }: FeatureTextSection) => {
  // const cta = (item: FeatureTextSectionLink) => {
  //   return (
  //     <div class="mt-8">
  //       <a
  //         href={item.link}
  //         class="p-5 bg-red-500 hover:bg-red-600 text-center text-gray-50 text-lg sm:text-2xl leading-9 rounded-md"
  //       >
  //         {item.linkText}
  //       </a>
  //     </div>
  //   );
  // };

  return (
    <div className="flex-column items-center justify-between">
      <div className="text-gray-50 text-2xl leading-9">
        <div className="flex grow">{text}</div>
      </div>
      {!!link && <FeatureCta item={link} />}
    </div>
  );
};
