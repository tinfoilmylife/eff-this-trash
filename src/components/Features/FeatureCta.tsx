import { FeatureTextSectionLink } from '@/util/types';
import Link from 'next/link';

interface FeatureCtaProps {
  item: FeatureTextSectionLink;
}

export const FeatureCta = ({ item }: FeatureCtaProps) => {
  return (
    <div className="mt-8">
      <Link
        href={item.link}
        className="p-5 bg-red-500 hover:bg-red-600 text-center text-gray-50 text-lg sm:text-2xl leading-9 rounded-md"
      >
        {item.linkText}
      </Link>
    </div>
  );
};
