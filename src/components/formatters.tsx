import { GoodWebsite } from '@/util/types';
import Image from 'next/image';
import { Fragment } from 'react';
import { HiExternalLink } from 'react-icons/hi';

export interface SupaLink {
  href: string;
  label: string;
}

interface ReasonItem {
  text: string;
  link?: string;
}

interface Reason {
  main: ReasonItem;
  extra: ReasonItem[];
}

const nestedList = (reason: Reason) => {
  if (reason?.extra?.length) {
    return (
      <ul className="list-inside ml-6" style={{ listStyleType: 'circle' }}>
        {reason.extra.map((xtra: ReasonItem, index: number) => {
          return (
            <li key={`${xtra.text.trim().substr(0, 12)}-${index}`}>
              <div className="inline">
                {xtra.text}
                {xtra.link && (
                  <a href={xtra.link} target="_blank">
                    <HiExternalLink className="text-orange-300 inline ml-1 w-4" />
                  </a>
                )}
              </div>
            </li>
          );
        })}
      </ul>
    );
  }
  return <></>;
};

export const formatters = {
  returnString: (arg: string | number | string[] | number[]) => {
    if (arg && Array.isArray(arg)) {
      return <span>{arg.join(', ')}</span>;
    }
    if (arg) {
      return <span>{arg.toString()}</span>;
    }
    return '';
  },
  returnJoin: (arg: string[] | number[]) => (
    <span>{arg?.length ? arg.join(', ') : ''}</span>
  ),
  returnMain: (arg: Record<string, unknown>) =>
    arg?.length && Array.isArray(arg) ? (
      <ul className="list-disc list-inside">
        {arg.map((reason, index) => {
          return (
            <Fragment key={`${reason.main.text.trim().substr(0, 12)}-${index}`}>
              <li>
                <div className="inline">
                  {reason.main.text}
                  {reason.main.link && (
                    <a href={reason.main.link} target="_blank">
                      <HiExternalLink className="text-orange-300 inline ml-1 w-4" />
                    </a>
                  )}
                </div>
                {nestedList(reason)}
              </li>
            </Fragment>
          );
        })}
      </ul>
    ) : (
      <></>
    ),
  returnCompany: (company: string, image?: string | null) => {
    if (image) {
      return (
        <div className="flex items-center md:w-[7rem] flex-wrap">
          <Image
            src={image}
            style={{
              maxWidth: '4rem',
              maxHeight: '4rem',
              marginRight: '.25rem',
            }}
            alt={company}
            width={200}
            height={200}
            loading="lazy"
          />
          <div className="font-bold ml-1 md:ml-0">{company.toString()}</div>
        </div>
      );
    }
    return <span>{company.toString()}</span>;
  },
  returnButtonDialog: (subs: string[]) => {
    const subsLen = subs?.length || 0;
    if (subsLen) {
      return subs?.length > 0 ? <div className="mt-2 md:mt-0"></div> : <></>;
    } else {
      return <></>;
    }
  },
  returnSiteLink: (website: GoodWebsite) => {
    if (website) {
      return (
        <a
          href={website.link}
          className="flex items-center underline hover:text-red-500"
          target="_blank"
        >
          {website.domain}
          <HiExternalLink />
        </a>
      );
    }
    return <></>;
  },
};
