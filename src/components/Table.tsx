'use client';

import { formatters } from './formatters';
import { ColumnData, Company, FiltersItem, Reason } from '@/util/types';
import { useMemo, useState } from 'react';
import { sortBy } from '@/util/sorts';
import { JointCatObj } from '@/util/constants';
import Search from './basic/Search';
import { HiTrash } from 'react-icons/hi';
import { theme } from '../../tailwind.config';
import Dialog from './basic/Dialog';
import Filters from './Filters/Filters';
import Image from 'next/image';

interface HeaderProps {
  text: string;
  sortable: boolean;
}

type SortDirs = 'desc' | 'asc';

interface TableProps {
  data: Company[];
  header: HeaderProps[];
  columns: ColumnData[];
  details: {
    defaultSortColumn: string;
    defaultSortDir: SortDirs;
  };
  showFilters?: boolean;
  categoriesList: JointCatObj;
}

export default function Table({
  data = [],
  header,
  columns,
  details,
  categoriesList,
}: TableProps) {
  const [sortCol, setSortCol] = useState(details.defaultSortColumn);
  const [sortDir, setSortDir] = useState(details.defaultSortDir);
  const [filteredResults, setFilteredResults] = useState<Company[]>(data);
  const [currentFilters, setCurrentFilters] = useState<FiltersItem[]>();

  const labelClasses = 'text-xl text-bold';

  const returnButtonDialog = (subs: string[], company: string) => {
    return (
      <Dialog
        title={`${company} Brands/Subsidiaries`}
        buttonText="SEE BRANDS"
        buttonVariant="alert"
      >
        <div className="grid grid-flow-row grid-cols-2 w-full text-left leading-5 font-normal text-xl">
          {subs.sort().map((sub) => (
            <div key={`${company}-${sub.split(' ').join('')}-dialog`}>
              {sub}
            </div>
          ))}
        </div>
      </Dialog>
    );
  };

  const formatScoreBad = (score: number, company: string) => {
    return (
      <div className="flex justify-start md:justify-center align-center mt-1 md:mt-0">
        {Array.apply(null, new Array(score)).map((s, index: number) => (
          <HiTrash
            className="text-red-500"
            style={{ width: '1rem', height: '1rem' }}
            key={`${company}-${index}-score`}
          />
        ))}
      </div>
    );
  };

  const sorted = useMemo(() => {
    const initialSort = sortBy(filteredResults, sortCol);
    if (sortDir === 'asc') {
      return initialSort;
    } else {
      return initialSort.reverse();
    }
  }, [filteredResults, sortCol, sortDir]);

  const roundCornersOfCell = (header: any[], index: number) => {
    if (index === 0) {
      return { borderTopLeftRadius: '.375rem' };
    }
    if (header.length === index + 1) {
      return { borderTopRightRadius: '.375rem' };
    }
    return {};
  };

  const formatCell = (col: ColumnData, rowData: Company) => {
    if (col.key === 'name') {
      return formatters.returnCompany(rowData.name, rowData.logo);
    } else if (col.format === 'returnButtonDialog' && !!rowData.subsidaries) {
      return returnButtonDialog(rowData.subsidaries, rowData.name);
    }
    // @ts-ignore
    else if (col.format === 'formatScoreBad') {
      return formatScoreBad(rowData.score, rowData.name);
    } else {
      // @ts-ignore later
      return formatters[col.format](rowData[col.key]);
    }
  };

  const filterResults = (val: string) => {
    const filtered = data.filter((item) => {
      const value = val.toLowerCase();
      const extra = item.reasons.length
        ? item.reasons
            .map((r) => (r.extra || []).map((e) => e.text).join(' '))
            .join(' ')
        : '';
      const reasons =
        // @ts-ignore
        (item.reasons || item.alternativeTo)
          ?.map((reason: Reason) => {
            return reason.main.text;
          })
          .join(' ')
          .toLowerCase() || '';
      const tags = (item.tags || []).join(' ');
      return (
        item.name.toLowerCase().includes(value) ||
        reasons.includes(value) ||
        extra.includes(value) ||
        tags.includes(value)
      );
    });
    setFilteredResults(filtered);
  };

  const catList = (cats: (keyof JointCatObj)[], company: string) => {
    return (
      <div className="flex justify-start md:justify-center mt-1 md:mt-0 items-start">
        {cats.sort().map((cat) => (
          <Image
            key={`${company}-${cat}`}
            src={categoriesList[cat]?.src || ''}
            alt={categoriesList[cat]?.alt || ''}
            title={categoriesList[cat]?.display || ''}
            style={{
              marginRight: '.1rem',
              marginLeft: '.1rem',
              maxWidth: '20px',
              maxHeight: '20px',
            }}
            height={30}
            width={30}
            loading="lazy"
          />
        ))}
      </div>
    );
  };

  const chipClicked = (fils: FiltersItem[]) => {
    setCurrentFilters(fils);
    const selected = fils.filter((fil) => fil.selected).map((i) => i.name);
    const filtered = data.filter((item) => {
      if (item.categories) {
        const hasCats = item?.categories.filter((cat) => {
          return selected.includes(cat);
        });
        return !!hasCats.length;
      }
      return false;
    });
    setFilteredResults(filtered);
  };

  return (
    <div className="w-full px-2 md:px-0">
      <div className="mb-4 flex-column md:flex-row">
        <label className={labelClasses}>Search</label>
        <Search
          searchFilter={filterResults}
          subText="* Search bar will search all results using company name, tags, and
          reasons columns to filter results."
        />
        <Filters onChipClick={chipClicked} allCats={categoriesList} />
      </div>
      <div className="w-full md:table-auto md:border-collapse rounded-t-md">
        <div className="hidden md:table-header-group bg-gray-950 text-red-500 rounded-t-md w-full">
          <div className="hidden md:table-row font-heavy">
            {!!header?.length &&
              header.map((th, i) => {
                return (
                  <div
                    className="table-cell font-bold p-2"
                    style={roundCornersOfCell(header, i)}
                    key={th.text}
                  >
                    {th.text}
                  </div>
                );
              })}
          </div>
        </div>
        <div className="flex-column md:table-row-group w-full">
          {!!sorted.length &&
            sorted.map((rowData, index) => {
              return (
                <div
                  className={`flex-column md:table-row ${
                    index % 2 === 0 ? 'bg-blue-900' : 'bg-blue-800'
                  } text-white${index === 0 ? ' rounded-t-md' : ''}`}
                  key={`${rowData.name}-row`}
                >
                  {columns.map((col: ColumnData, index: number) => {
                    return (
                      <div
                        className={`${
                          col.key === 'reasons'
                            ? 'flex-column'
                            : 'flex flex-row'
                        } align-center md:table-cell p-3 align-middle min-w-[6rem]${
                          col.key === 'reasons' ? ' w-full' : ''
                        }${
                          (col.key === 'subsidaries' && !rowData.subsidaries) ||
                          (col.key === 'alternatives' && !rowData.alternatives)
                            ? ' hidden'
                            : ''
                        }`}
                        key={`${rowData.name}-${col.key}-col`}
                      >
                        <div
                          className={`md:hidden font-bold text-underline font-heavy text-orange-300`}
                        >
                          {header[index].text !== 'Company'
                            ? header[index].text
                            : ''}
                        </div>
                        <div
                          className={`flex-row md:flex-column ${
                            col.key === 'name' ? 'ml-0' : 'ml-2'
                          } md:ml-0`}
                        >
                          {col.key === 'categories' ? (
                            catList(rowData.categories, rowData.name)
                          ) : (
                            <div>{formatCell(col, rowData)}</div>
                          )}
                        </div>
                        {index < columns.length - 1 ? (
                          <hr
                            className="md:hidden mt-2"
                            style={{
                              // @ts-ignore
                              borderTop: `1px solid ${theme.extend.colors.blue['950']}`,
                            }}
                          />
                        ) : (
                          <></>
                        )}
                      </div>
                    );
                  })}
                </div>
              );
            })}
          {!sorted?.length && (
            <div className="w-full flex justify-center py-4 text-lg">
              No results to display (make sure at least one category is
              selected)
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
