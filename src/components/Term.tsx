import Card from './basic/Card';

interface TermProps {
  word: string;
  description: string;
  phonetics: string;
  type: string;
}

export default function Term({
  word,
  description,
  phonetics,
  type,
}: TermProps) {
  return (
    <Card bgClass="bg-gray-950 my-8 px-2 md:px-8" corners="md">
      <article>
        <div className="w-full">
          <h3 className="text-red-500 mb-2 text-3xl">{word}</h3>
        </div>
        <div className="w-full text-orange-500 text-xl italic">
          {`(${type}): ${phonetics}`}
        </div>
        <p className="text-gray-50 leading-7 text-xl">{description}</p>
      </article>
    </Card>
  );
}
