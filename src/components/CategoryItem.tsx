import { Category } from '@/util/constants';
import Image from 'next/image';

interface CategoryItemProps {
  category: Category;
}

export default function CategoryItem({ category }: CategoryItemProps) {
  return (
    <article className="w-full lg:grid lg:grid-cols-10 gap-8 content-center mt-16 px-2">
      <div className="flex item-center content-center col-span-2 mb-2 md:mb-0">
        <div style={{ width: '30px', height: '30px' }} className="mr-4">
          <Image src={category.src} alt={category.alt} width={30} height={30} />
        </div>
        <h3 className="text-red-500">{category.display}</h3>
      </div>
      <p className="text-gray-50 leading-7 text-xl col-span-8 initial-letter">
        {category.description}
      </p>
    </article>
  );
}
