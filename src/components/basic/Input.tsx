interface InputProps {
  classes: string;
  isDisabled?: boolean;
  props: HTMLInputElement;
}

const Input: React.FC<InputProps> = ({ classes, isDisabled, ...props }) => {
  return (
    <input
      {...props}
      disabled={isDisabled}
      className={`px-3 py-2 rounded-md bg-gray-200 text-black active:shadow-md border(gray-500 2) disabled:(opacity-50 cursor-not-allowed) ${
        classes ?? ''
      }`}
      style={{ width: '20rem', maxWidth: '100%' }}
    />
  );
};

export default Input;
