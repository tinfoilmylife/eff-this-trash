import { ReactNode } from 'react';
import { JsxElement } from 'typescript';

export type ButtonVariant =
  | 'primary'
  | 'secondary'
  | 'warning'
  | 'success'
  | 'alert'
  | 'accent'
  | 'chip';

interface ButtonProps {
  variant: ButtonVariant;
  classes?: string;
  children: ReactNode;
  isDisabled?: boolean;
  styles?: object;
  onButtonClick: (arg?: any) => void;
}

export function Button({
  variant,
  classes,
  children,
  isDisabled,
  styles,
  onButtonClick,
}: ButtonProps) {
  const buttonOptions = {
    primary: {
      bg: 'bg-blue-900',
      fg: 'text-gray-50',
      hover: 'hover:bg-blue-950',
    },
    secondary: {
      bg: 'bg-gray-900',
      fg: 'text-gray-50',
      hover: 'hover:bg-gray-950',
    },
    warning: {
      bg: 'bg-orange-600',
      fg: 'text-gray-950',
      hover: 'hover:bg-orange-500',
    },
    success: {
      bg: 'bg-green-700',
      fg: 'text-gray-950',
      hover: 'hover:bg-green-800',
    },
    alert: {
      bg: 'bg-red-500',
      fg: 'text-gray-50',
      hover: 'hover:bg-red-600',
    },
    accent: {
      bg: 'bg-teal-700 line leading-4 drop-shadow-md',
      fg: 'text-gray-50',
      hover: 'hover:bg-teal-700',
    },
    chip: {
      bg: 'bg-teal-600 rounded-full',
      fg: 'text-gray-50',
      hover: 'hover:bg-teal-700',
    },
  };

  return (
    <button
      style={styles || undefined}
      disabled={isDisabled}
      className={`px-2 py-1 border(gray-100 2) rounded-md shadow-md ${
        classes || ''
      } ${buttonOptions[variant].bg} ${buttonOptions[variant].fg} ${
        buttonOptions[variant].hover
      }`}
      onClick={onButtonClick}
    >
      {children}
    </button>
  );
}
