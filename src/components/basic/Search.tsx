import { HiSearch } from 'react-icons/hi';

interface SearchProps {
  searchFilter: (val: string) => void;
  subText?: string;
  classes?: string;
}

export default function Search({
  searchFilter,
  subText,
  classes,
}: SearchProps) {
  return (
    <div>
      <label aria-label="search" className="flex flex-row items-center">
        <div
          className="m-0 bg-teal-800 p-2"
          style={{
            borderTopLeftRadius: '.25rem',
            borderBottomLeftRadius: '.25rem',
          }}
        >
          <HiSearch style={{ width: '2.25rem', height: '2.25rem' }} />
        </div>
        <input
          className={`p-3 text-lg bg-gray-800 text-gray-50 ${classes || ''}`}
          style={{
            borderTopRightRadius: '.25rem',
            borderBottomRightRadius: '.25rem',
          }}
          onKeyUp={(e: any) => searchFilter(e.target.value)}
        />
      </label>
      <div className="text-gray-50">
        {subText && <div className="italic mt-1">{subText}</div>}
      </div>
    </div>
  );
}
