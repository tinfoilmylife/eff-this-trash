import { ReactNode, useEffect, useRef, useState } from 'react';
import { Button, ButtonVariant } from './Button';
import { HiX } from 'react-icons/hi';
import { theme } from '../../../tailwind.config';

interface DialogProps {
  buttonText: string;
  buttonVariant: ButtonVariant;
  children: ReactNode;
  title?: string;
}

const Dialog: React.FC<DialogProps> = ({
  buttonText,
  buttonVariant,
  children,
  title,
}: DialogProps) => {
  // just using this to potentially prevent dialog flash on load
  const [isShowing, setIsShowing] = useState(false);
  const dialogRef = useRef<HTMLDialogElement>(null);
  const showDialog = (event: MouseEvent) => {
    // @ts-ignore general
    if (dialogRef?.current && event.target.tagName === 'BUTTON') {
      setIsShowing(true);
      dialogRef.current.showModal();
    }
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (
      dialogRef.current &&
      // @ts-ignore general
      event?.target?.getAttribute('id') !== 'dialog-wrapper'
    ) {
      setIsShowing(false);
      dialogRef.current.close();
    } else {
      event.stopPropagation();
      return;
    }
  };

  useEffect(() => {
    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dialogRef]);

  return (
    <Button
      variant={buttonVariant}
      onButtonClick={showDialog}
      styles={{
        fontWeight: 'bold',
        paddingTop: '.5rem',
        paddingBottom: '.5rem',
        // @ts-ignore
        border: `1px solid ${theme.colors.white}`,
      }}
      classes="line leading-4 drop-shadow-md"
    >
      {buttonText}
      <dialog
        className="p-4 rounded-md w-full max-w-4xl max-h-fit shadow-lg bg-blue-800 text-gray-50 max-h-full overflow-y-auto"
        style={{ display: isShowing ? 'block' : 'none' }}
        ref={dialogRef}
      >
        <div className="w-full flex flex-row justify-end">
          <HiX
            style={{
              width: '2rem',
              height: '2rem',
              display: isShowing ? 'block' : 'none',
            }}
            onClick={() => {
              setIsShowing(false);
              dialogRef?.current?.close();
            }}
            role="button"
            aria-label="close modal"
          />
        </div>
        <div
          id="dialog-wrapper"
          style={{ display: isShowing ? 'block' : 'none' }}
        >
          <div className="w-full text-align-center mb-4">
            <h2 className="text-red-500 leading-7">{title}</h2>
          </div>
          {children}
        </div>
      </dialog>
    </Button>
  );
};

export default Dialog;
