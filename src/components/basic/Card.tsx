import { ReactNode } from 'react';

type CornerOptions = 'sm' | 'md' | 'lg';

interface CardProps {
  corners: CornerOptions;
  bgClass: string;
  children: ReactNode;
}

export default function Card({ corners, bgClass, children }: CardProps) {
  return (
    <div className={'shadow-md p-6' + ` rounded-${corners} ${bgClass}`}>
      {children}
    </div>
  );
}
