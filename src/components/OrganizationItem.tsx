import { Organization } from '@/util/organizations';
import Image from 'next/image';
import { HiExternalLink } from 'react-icons/hi';

interface OrganizationItemProps {
  org: Organization;
}

export default function OrganizationItem({ org }: OrganizationItemProps) {
  return (
    <article className="w-full lg:grid lg:grid-cols-10 gap-8 content-center mt-16 px-2">
      <div className="content-center col-span-3">
        <h3 className="text-red-500 flex items-center mb-2">
          {org.name}
          <a href={org.link} target="_blank" className="ml-2 text-orange-500">
            <HiExternalLink />
          </a>
        </h3>
        <div style={{ width: '100%' }} className="mr-4">
          <Image
            src={org.logo}
            alt={`${org.name} logo`}
            width={400}
            height={org.imgHeight}
            loading="lazy"
          />
        </div>
      </div>
      <p className="text-gray-50 leading-7 text-xl col-span-7 initial-letter mt-2 md:mt-0">
        {org.description}
      </p>
    </article>
  );
}
