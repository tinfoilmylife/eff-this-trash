'use client';

import { Button } from '../basic/Button';
import { useEffect, useState } from 'react';
import { HiChevronDoubleUp } from 'react-icons/hi';
import { theme } from '../../../tailwind.config';

export default function ScrollToTop() {
  const [showScrollTop, setShowScrollTop] = useState<boolean>(false);
  const handleClick = () => {
    const body = document.querySelector('#root');

    if (body) {
      body.scrollIntoView({
        behavior: 'smooth',
      });
    }
  };

  useEffect(() => {
    const showOrHide = () => {
      if (window.scrollY > 50) {
        setShowScrollTop(true);
      } else {
        setShowScrollTop(false);
      }
    };

    document.addEventListener('scroll', showOrHide);

    return () => document.removeEventListener('scroll', showOrHide);
  });

  return (
    <Button
      classes="scroll-top-button right-2 sm:right-8 lg:right-8 xl:right-16 bottom-10 sm:bottom-6"
      variant="accent"
      styles={{
        position: 'fixed',
        zIndex: 8,
        width: '2.5rem',
        height: '2.5rem',
        display: showScrollTop ? 'flex' : 'none',
        justifyContent: 'center',
        alignItems: 'center',
        // @ts-ignore
        border: `1px solid ${theme?.colors?.gray['50'] || '#fff'}`,
      }}
      onButtonClick={handleClick}
    >
      <HiChevronDoubleUp />
    </Button>
  );
}
