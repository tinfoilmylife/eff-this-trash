import { ReactNode } from 'react';

interface WrapperProps {
  children: ReactNode;
}

export default function Wrapper({ children }: WrapperProps) {
  return (
    <div className="pt-4 pb-8 px-2 sm:px-4 lg:px-8 xl:px-16">{children}</div>
  );
}
