'use client';

import {
  HiMenu,
  HiOutlineHome,
  HiTrash,
  HiStar,
  HiInformationCircle,
  HiChevronDown,
} from 'react-icons/hi';
import { theme } from '../../../tailwind.config';
import { Fragment, useEffect, useRef, useState } from 'react';
import effThisLight from '../../../public/effThis-light.png';
import Image from 'next/image';
import Link from 'next/link';

type Props = {
  active: string;
  activeChild?: string;
};

export default function Header({ active, activeChild }: Props) {
  const [menuOpen, setMenuOpen] = useState<boolean>(false);
  const menus = [
    { name: 'Home', href: '/', icon: HiOutlineHome },
    {
      name: 'Info',
      href: '/info',
      icon: HiInformationCircle,
      children: [
        { label: 'General', href: '/info' },
        { label: 'Terminology', href: '/terminology' },
        { label: 'Evil Organizations', href: '/evilorgs' },
        { label: 'Good Organizations', href: '/goodorgs' },
      ],
    },
    {
      name: 'Garbage brands',
      href: '/garbagebrands',
      icon: HiTrash,
    },
    { name: 'Good brands', href: '/goodbrands', icon: HiStar },
  ];
  const sideMenuRef = useRef<HTMLDivElement>(null);
  const hamburgerRef = useRef<HTMLButtonElement>(null);

  const menuToggle = () => {
    setMenuOpen(!menuOpen);
  };

  const handleClickOutside = (event: MouseEvent) => {
    console.log(event.target instanceof HTMLElement);
    if (
      // button was clicked; let menuToggle do the work
      // @ts-ignore
      hamburgerRef.current?.contains(event.target)
    ) {
      return;
    } else if (
      // clicked inside; do not close
      event.target instanceof HTMLElement &&
      sideMenuRef.current?.contains(event.target)
    ) {
      return;
    } else {
      // clicked outside; close
      setMenuOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [sideMenuRef]);

  return (
    <Fragment>
      <div className="w-full flex flex-row mb-8 bg-gray-950 py-4 px-2 sm:px-4 lg:px-8 xl:px-16">
        <div className="flex items-center flex-1">
          <Link href="/">
            <Image
              src={effThisLight}
              style={{
                width: '5rem',
                transition: 'width 0.2s ease-out',
              }}
              alt="trash can"
              priority
            />
          </Link>
        </div>
        <nav className="flex items-center">
          <ul className="md:flex items-center gap-6 hidden md:visible">
            {menus.map((menu) => (
              <li key={menu.name}>
                {!!menu.children?.length && (
                  <div className="dropdown">
                    <div
                      className={
                        'dropdown-trigger py-1 px-2 border-red-500 flex justify-center items-center' +
                        (menu.href === active
                          ? ' font-bold border-b-2 text-gray-50'
                          : ' text-gray-100 hover:text-red-500')
                      }
                    >
                      <menu.icon className="mr-1 h-4 w-4" />
                      {menu.name}
                      <HiChevronDown className="ml-1 dd-caret" />
                    </div>
                    <div className="dropdown-content py-1 px-2 bg-gray-900 text-gray-50 drop-shadow-lg">
                      {menu.children.map((child, index) => {
                        return (
                          <Fragment key={child.label}>
                            <div className="my-2">
                              <Link
                                href={child.href}
                                className={`font-bold hover:text-red-300${
                                  activeChild && activeChild === child.label
                                    ? ' text-red-400'
                                    : ''
                                }`}
                              >
                                {child.label}
                              </Link>
                            </div>
                            {index < menu.children.length - 1 && <hr />}
                          </Fragment>
                        );
                      })}
                    </div>
                  </div>
                )}
                {!menu.children && (
                  <a
                    href={menu.href}
                    className={
                      'py-1 px-2 border-red-500 flex justify-center items-center' +
                      (menu.href === active
                        ? ' font-bold border-b-2 text-gray-50'
                        : ' text-gray-100 hover:text-red-500')
                    }
                  >
                    <menu.icon className="mr-1 h-4 w-4" />
                    {menu.name}
                  </a>
                )}
              </li>
            ))}
          </ul>
        </nav>
        <div
          className="sm:block md:hidden"
          style={{
            marginRight: menuOpen ? '16rem' : 0,
            transition: 'margin-right 0.2s ease-out',
            zIndex: 10,
          }}
        >
          <button
            className="bg:transparent border:none mt-3"
            onClick={menuToggle}
            style={{ outline: 'none' }}
            ref={hamburgerRef}
          >
            <HiMenu
              style={{
                transform: menuOpen ? 'rotate(-90deg)' : 'rotate(0deg)',
                transition: 'transform,color 0.2s ease-out',
                color: menuOpen
                  ? // @ts-ignore
                    theme?.colors?.white
                  : // @ts-ignore
                    theme.colors.red,
              }}
              className="text-3xl"
            />
          </button>
        </div>
      </div>
      <div className={'overlay' + (menuOpen ? ' active' : '')}></div>
      <div
        className="sidebar bg-blue-950"
        style={{
          transform: menuOpen ? 'translateX(0)' : 'translateX(100%)',
          transition: 'transform 0.2s ease-out',
          boxShadow: menuOpen ? '-5px 1px 5px -1px rgba(0,0,0,0.42)' : 'none',
        }}
        ref={sideMenuRef}
      >
        <Image
          src={effThisLight}
          style={{
            width: '6rem',
            transition: 'width 0.2s ease-out',
            marginBottom: '2rem',
            marginTop: '1rem',
          }}
          alt="trash can"
        />
        <nav className="flex-col items-center">
          {menus.map((menu) => (
            <Fragment key={menu.name}>
              {!!menu.children?.length && (
                <>
                  <div className="mb-2">
                    <Link
                      href={menu.href}
                      className={
                        'py-2 px-2 border-gray-200 text-xl flex rounded-md' +
                        (menu.href === active
                          ? ' font-bold bg-red-500 text-gray-50'
                          : ' text-gray-100 hover:text-red-500')
                      }
                    >
                      <menu.icon className="mr-2" /> <span>{menu.name}</span>
                    </Link>
                  </div>
                  <ul className="text-gray-100 pl-8 mb-6 text-lg list-disc">
                    {menu.children?.map((child) => (
                      <li
                        key={child.label}
                        className={`my-2 rounded-md px-2${
                          activeChild && activeChild === child.label
                            ? ' bg-red-500'
                            : ''
                        }`}
                      >
                        <a
                          href={child.href}
                          className={`font-bold hover:text-red-300${
                            activeChild && activeChild === child.label
                              ? ' text-gray-50'
                              : ''
                          }`}
                        >
                          {child.label}
                        </a>
                      </li>
                    ))}
                  </ul>
                </>
              )}
              {!menu.children && (
                <div className="mb-6">
                  <Link
                    href={menu.href}
                    className={
                      'py-2 px-2 border-gray-200 text-xl flex rounded-md' +
                      (menu.href === active
                        ? ' font-bold bg-red-500 text-gray-50'
                        : ' text-gray-100 hover:text-red-500')
                    }
                  >
                    <menu.icon className="mr-2" /> <span>{menu.name}</span>
                  </Link>
                </div>
              )}
            </Fragment>
          ))}
        </nav>
      </div>
    </Fragment>
  );
}
