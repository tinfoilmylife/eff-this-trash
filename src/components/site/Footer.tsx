import Image from 'next/image';
import { AiOutlineGitlab } from 'react-icons/ai';
import effThisLight from '../../../public/effThis-light.png';

export default function Footer() {
  const menus = [
    {
      title: 'Eff This Trash',
      children: [
        { name: 'About', href: '/about' },
        { name: 'Privacy Policy', href: '/privacypolicy' },
      ],
    },
    {
      title: 'Other sites',
      children: [
        {
          name: 'CancelThisCompany.com',
          href: 'https://www.cancelthiscompany.com/',
        },
        {
          name: 'Concerned Women for America',
          href: 'https://concernedwomen.org/alternative-options-to-woke-companies-funding-abortion-travel-for-employees/',
        },
        {
          name: 'PIRG',
          href: 'https://pirg.org/articles/who-doesnt-want-the-right-to-repair-companies-worth-over-10-trillion/',
        },
        {
          name: 'iFixIt',
          href: 'https://www.ifixit.com/Right-to-Repair',
        },
        {
          name: 'Tinfoil My Life',
          href: 'https://tinfoilmylife.com',
        },
        {
          name: 'Public Sq',
          href: 'https://app.publicsq.com/',
        },
      ],
    },
  ];

  return (
    <div style={{ display: 'block', position: 'relative' }}>
      <div
        className="bg-gray-950 flex flex-col md:flex-row w-full gap-8 justify-center md:gap-16 px-8 py-8 text-md mt-4"
        style={{ bottom: 0 }}
      >
        <div className="text-gray-200 space-y-2">
          <Image
            src={effThisLight}
            style={{ width: '4rem' }}
            alt="trash can"
            loading="lazy"
          />
          <div className="text-xs">
            Copyright © 2023 EffThisTrash.com
            <br />
            <br />
            All right reserved.
            <br />
            <br />
            Created by Tinfoil Guy
            <br />
            <br />
            <a
              href="https://gitlab.com/tinfoilmylife/eff-this-trash"
              target="_blank"
              className="flex items-center hover:text-teal-400"
            >
              {`See the code`}
              <AiOutlineGitlab className="ml-1" />
            </a>
          </div>
        </div>

        {menus.map((item, index) => (
          <div className="mb-4" key={item.title}>
            <div className="font-bold text-red-500">{item.title}</div>
            <ul className="mt-2">
              {item.children.map((child) => (
                <li className="mt-2" key={child.name}>
                  <a
                    className="text-gray-100 hover:text-teal-400"
                    href={child.href}
                    target={index === 0 ? undefined : '_blank'}
                  >
                    {child.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </div>
  );
}
