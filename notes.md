# Notes

## no straight quotes

- double quotes opening
  - `\u201c`
  - `&ldquo;`
- double quotes closing
  - `\u201d`
  - `&rdquo;`
- apostrophe
  - `\u0027`
  - `&apos;`
- single quote opening
  - `\u2018`
  - `&lsquo;`
- single quote closing
  - `\u2019`
  - `&rsquo;`

## mobile issues

### general

- ~~hamburger menu too small~~
- ~~scroll to top button too low on mobile~~
- ~~resized, the hamburger menu closes when clicked but in mobile emulator it does not~~

### About

- ~~ul has extra spacing and should on left, but not on right too~~
- ~~rest of it all has too much padding~~

### Info

- ~~mobile view some category icons overlap text below~~

### Terminology

- ~~too much padding~~
- ~~messes up mobile nav~~
- ~~scroll to top button off right of screen~~

### brands views

- ~~mobile view dialog title spacing issue when title is long (look at miller)~~

### make contact view

- there is reference to this view in the privacy policy, and I think I have to have something there for it according to GDPR

### GDPR banner

- because the EU has become a commie hellscape
